import numpy as np
import pytest

import daltonproject as dp
from daltonproject import natural_occupation


@pytest.mark.datafiles(pytest.DATADIR / 'mp2_Ethylene.out')
def test_pick_cas_by_thresholds(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'mp2_Ethylene')
    ref_electrons, ref_cas, ref_inactive = (4, [0, 1, 1, 0, 0, 1, 1, 0], [3, 1, 0, 0, 2, 0, 0, 0])
    electrons, cas, inactive = natural_occupation.pick_cas_by_thresholds(output.natural_occupations, 1.98, 0.01)
    assert electrons == ref_electrons
    assert cas == ref_cas
    assert inactive == ref_inactive


@pytest.mark.datafiles(pytest.DATADIR / 'mp2_He.out')
def test_trimmed_natural_occ(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'mp2_He')
    out_string = natural_occupation.trim_natural_occupations(output.natural_occupations)
    ref_string = 'Symmetry: 1\n\n1.9909   0.0048   \n\nSymmetry: 2\n\n\nSymmetry: 3\n\n\nSymmetry: ' \
                 '4\n\n\nSymmetry: 5\n\n\nSymmetry: 6\n\n\nSymmetry: 7\n\n\nSymmetry: 8\n'
    assert out_string == ref_string
    out_string = natural_occupation.trim_natural_occupations(
        np.array([1.9999, 1.8, 1.7, 1.6, 0.1, 0.2, 1.8, 1.7, 1.6, 0.1, 0.2, 0.1, 0.2]))
    ref_string = 'Symmetry: 1\n\n1.8000   1.8000   1.7000   1.7000   1.6000   \n1.6000   0.2000   0.2000   ' \
                 '0.2000   0.1000   \n0.1000   0.1000   '
    assert out_string == ref_string


@pytest.mark.datafiles(
    pytest.DATADIR / 'mp2_He.out',
    pytest.DATADIR / 'mp2srdft_Be.out',
)
def test_scan_occupations(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'mp2_He')
    out_string = natural_occupation.scan_occupations(output.natural_occupations)
    ref_string = 'Strongly occupied natural orbitals                      Weakly occupied natural ' \
                 'orbitals\nSymmetry   Occupation   Change in occ.   Diff. to 2     Symmetry   Occupation   Change ' \
                 'in occ.\n   1          1.9909        0.0000        0.0091            1          0.0048        ' \
                 '0.0000\n'
    assert out_string == ref_string
    # Test that the scan works when n_active < n_virtual, with n_virtual having values larger than 0.01.
    output = dp.dalton.OutputParser(datafiles / 'mp2srdft_Be')
    out_string = natural_occupation.scan_occupations(output.natural_occupations)
    ref_string = 'Strongly occupied natural orbitals                      Weakly occupied natural ' \
                 'orbitals\nSymmetry   Occupation   Change in occ.   Diff. to 2     Symmetry   Occupation   Change ' \
                 'in occ.\n   1          1.9648        0.0000        0.0352            1          0.0112        ' \
                 '0.0000\n---------------------------------------------------         1          0.0112        ' \
                 '0.0000\n---------------------------------------------------         1          0.0112        ' \
                 '0.0000\n'
    assert out_string == ref_string
    # Test that scan works when n_active > n_virtual.
    out_string = natural_occupation.scan_occupations(np.array([1.8, 1.7, 1.6, 0.1, 0.2]))
    ref_string = 'Strongly occupied natural orbitals                      Weakly occupied natural orbitals\n' \
                 'Symmetry   Occupation   Change in occ.   Diff. to 2     Symmetry   Occupation   Change in ' \
                 'occ.\n   1          1.6000        0.0000        0.4000            1          0.2000        ' \
                 '0.0000\n   1          1.7000        0.1000        0.3000            1          0.1000        ' \
                 '0.1000\n   1          1.8000        0.1000        0.2000   ' \
                 '--------------------------------------------\n'
    assert out_string == ref_string
    # Test that max_orbitals work.
    out_string = natural_occupation.scan_occupations(np.array([1.8, 1.7, 1.6, 0.1, 0.2]), max_orbitals=1)
    ref_string = 'Strongly occupied natural orbitals                      Weakly occupied natural ' \
                 'orbitals\nSymmetry   Occupation   Change in occ.   Diff. to 2     Symmetry   Occupation   ' \
                 'Change in occ.\n   1          1.6000        0.0000        0.4000            1          ' \
                 '0.2000        0.0000\n'
    assert out_string == ref_string


def test_sort_natural_occupations_exceptions():
    with pytest.raises(TypeError,
                       match='natural_occupations must either be a NumPy array or a dict of NumPy arrays'):
        natural_occupation.sort_natural_occupations((2.0, 0.0))
