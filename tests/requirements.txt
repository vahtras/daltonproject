pre-commit==2.19.0
pytest==7.1.2
pytest-cov==3.0.0
pytest-datafiles==2.0.1
