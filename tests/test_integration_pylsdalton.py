import numpy as np
import pytest

import daltonproject as dp
from daltonproject.utilities import chdir


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz', )
def test_pylsdalton_dimensions(datafiles):
    """Dimensions."""
    with chdir(datafiles):
        b3lyp = dp.QCMethod('DFT', 'B3LYP')
        basis = dp.Basis(basis='pcseg-0', ri='def2-universal-JKFIT', admm='admm-1')
        water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
        num_atom = dp.lsdalton.num_atoms(water, basis, b3lyp)
        num_electrons = dp.lsdalton.num_electrons(water, basis, b3lyp)
        num_basis = dp.lsdalton.num_basis_functions(water, basis, b3lyp)
        num_ri = dp.lsdalton.num_ri_basis_functions(water, basis, b3lyp)
    assert num_atom == 3
    assert num_electrons == 10
    assert num_basis == 13
    assert num_ri == 113


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz',
    pytest.DATADIR / 'lspy_overlap.npy',
    pytest.DATADIR / 'lspy_kinetic.npy',
    pytest.DATADIR / 'lspy_nucel.npy',
)
def test_pylsdalton_1el(datafiles):
    """One-electron matrices.

    --- code example to generate new reference data ---
    overlap = dp.lsdalton.overlap_matrix(water, basis, b3lyp)
    np.save('/path/to/daltonproject/tests/data/lspy_overlap.npy', overlap)
    --- end code example to generate new reference data ---
    """
    with chdir(datafiles):
        b3lyp = dp.QCMethod('DFT', 'B3LYP')
        basis = dp.Basis(basis='pcseg-0', ri='def2-universal-JKFIT', admm='admm-1')
        water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
        overlap = dp.lsdalton.overlap_matrix(water, basis, b3lyp)
        kinetic = dp.lsdalton.kinetic_energy_matrix(water, basis, b3lyp)
        nucel = dp.lsdalton.nuclear_electron_attraction_matrix(water, basis, b3lyp)
        ref_overlap = np.load(f'{datafiles}/lspy_overlap.npy')
        ref_kinetic = np.load(f'{datafiles}/lspy_kinetic.npy')
        ref_nucel = np.load(f'{datafiles}/lspy_nucel.npy')
    np.testing.assert_allclose(overlap, ref_overlap)
    np.testing.assert_allclose(kinetic, ref_kinetic)
    np.testing.assert_allclose(nucel, ref_nucel)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz',
    pytest.DATADIR / 'lspy_overlap.npy',
    pytest.DATADIR / 'lspy_h1density.npy',
    pytest.DATADIR / 'lspy_kinetic.npy',
    pytest.DATADIR / 'lspy_nucel.npy',
)
def test_pylsdalton_diag(datafiles):
    """Diagonalization.

    H1START constructed from the core hamiltonian MOs - i.e. by
    diagonalization of the 1-electron matrix, and constructing the
    AO density matrix from the occupied MOs.
    """
    with chdir(datafiles):
        b3lyp = dp.QCMethod('DFT', 'B3LYP')
        basis = dp.Basis(basis='pcseg-0', ri='def2-universal-JKFIT', admm='admm-1')
        water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
        density_ref = np.load(f'{datafiles}/lspy_h1density.npy')
        overlap = np.load(f'{datafiles}/lspy_overlap.npy')
        kinetic = np.load(f'{datafiles}/lspy_kinetic.npy')
        nucel = np.load(f'{datafiles}/lspy_nucel.npy')
        one_el = kinetic + nucel
        density = dp.lsdalton.diagonal_density(one_el, overlap, water, basis, b3lyp)
    np.testing.assert_allclose(density, density_ref, atol=1e-12)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz',
    pytest.DATADIR / 'lspy_h1density.npy',
    pytest.DATADIR / 'lspy_kinetic.npy',
    pytest.DATADIR / 'lspy_nucel.npy',
    pytest.DATADIR / 'lspy_coulomb.npy',
    pytest.DATADIR / 'lspy_exchange.npy',
    pytest.DATADIR / 'lspy_exchange_correlation.npy',
    pytest.DATADIR / 'lspy_fock.npy',
)
def test_pylsdalton_2el(datafiles):
    """Two-electron matrices."""
    with chdir(datafiles):
        b3lyp = dp.QCMethod('DFT', 'B3LYP')
        basis = dp.Basis(basis='pcseg-0', ri='def2-universal-JKFIT', admm='admm-1')
        water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
        ref_coulomb = np.load(f'{datafiles}/lspy_coulomb.npy')
        ref_exchange = np.load(f'{datafiles}/lspy_exchange.npy')
        ref_exchange_correlation = np.load(f'{datafiles}/lspy_exchange_correlation.npy')
        ref_fock = np.load(f'{datafiles}/lspy_fock.npy')
        density = np.load(f'{datafiles}/lspy_h1density.npy')
        kinetic = np.load(f'{datafiles}/lspy_kinetic.npy')
        nucel = np.load(f'{datafiles}/lspy_nucel.npy')
        one_el = kinetic + nucel
        coulomb = dp.lsdalton.coulomb_matrix(density, water, basis, b3lyp)
        exchange = dp.lsdalton.exchange_matrix(density, water, basis, b3lyp)
        xc_energy, xc_matrix = dp.lsdalton.exchange_correlation(density, water, basis, b3lyp)
        fock = dp.lsdalton.fock_matrix(density, water, basis, b3lyp)
        alt_fock = one_el + 2.0*coulomb - exchange + xc_matrix
    np.testing.assert_allclose(coulomb, ref_coulomb, atol=1e-12)
    np.testing.assert_allclose(exchange, -ref_exchange, atol=1e-12)
    np.testing.assert_allclose(xc_matrix, ref_exchange_correlation, atol=1e-12)
    np.testing.assert_allclose(fock, ref_fock, atol=1e-12)
    np.testing.assert_allclose(alt_fock, ref_fock, atol=1e-12)


@pytest.mark.datafiles(
    pytest.DATADIR / 'Hydrogen.xyz',
    pytest.DATADIR / 'lspy_eri4.npy',
    pytest.DATADIR / 'lspy_ri3.npy',
    pytest.DATADIR / 'lspy_ri2.npy',
)
def test_pylsdalton_ri(datafiles):
    """(E)RI Integrals."""
    with chdir(datafiles):
        b3lyp = dp.QCMethod('DFT', 'B3LYP')
        h2 = dp.Molecule(input_file=(datafiles / 'Hydrogen.xyz'))
        b2 = dp.Basis(basis='pcseg-0', ri='pcseg-1', admm='admm-1')
        ref_eri4 = np.load(f'{datafiles}/lspy_eri4.npy')
        ref_ri3 = np.load(f'{datafiles}/lspy_ri3.npy')
        ref_ri2 = np.load(f'{datafiles}/lspy_ri2.npy')
        eri4 = dp.lsdalton.eri4(h2, b2, b3lyp)
        ri3 = dp.lsdalton.ri3(h2, b2, b3lyp)
        ri2 = dp.lsdalton.ri2(h2, b2, b3lyp)
    np.testing.assert_allclose(eri4, ref_eri4, atol=1e-12)
    np.testing.assert_allclose(ri3, ref_ri3, atol=1e-12)
    np.testing.assert_allclose(ri2, ref_ri2, atol=1e-12)


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz', )
def test_pylsdalton_nuclear_energy(datafiles):
    with chdir(datafiles):
        water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
        nuclear_energy = dp.lsdalton.nuclear_energy(water)
    assert abs(nuclear_energy - 9.81947650946695) < 1e-14


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz',
    pytest.DATADIR / 'ep_points.npy',
    pytest.DATADIR / 'lspy_h1density.npy',
    pytest.DATADIR / 'electronic_ep.npy',
)
def test_pylsdalton_electronic_electrostatic_potential(datafiles):
    with chdir(datafiles):
        b3lyp = dp.QCMethod('DFT', 'B3LYP')
        basis = dp.Basis(basis='pcseg-0', ri='def2-universal-JKFIT', admm='admm-1')
        water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
        points = np.load(f'{datafiles}/ep_points.npy')
        density = np.load(f'{datafiles}/lspy_h1density.npy')
        ep_ref = np.load(f'{datafiles}/electronic_ep.npy')
        ep = dp.lsdalton.electronic_electrostatic_potential(density,
                                                            points,
                                                            water,
                                                            basis,
                                                            b3lyp,
                                                            ep_derivative_order=(0, 1))
    np.testing.assert_allclose(ep, ep_ref, atol=1e-12)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz',
    pytest.DATADIR / 'ep_points.npy',
    pytest.DATADIR / 'nuclear_ep.npy',
)
def test_pylsdalton_nuclear_electrostatic_potential(datafiles):
    with chdir(datafiles):
        water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
        points = np.load(f'{datafiles}/ep_points.npy')
        ep_ref = np.load(f'{datafiles}/nuclear_ep.npy')
        ep = dp.lsdalton.nuclear_electrostatic_potential(points, water, ep_derivative_order=(0, 1))
    np.testing.assert_allclose(ep, ep_ref, atol=1e-12)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz',
    pytest.DATADIR / 'ep_points.npy',
    pytest.DATADIR / 'lspy_h1density.npy',
    pytest.DATADIR / 'ep.npy',
)
def test_pylsdalton_electrostatic_potential(datafiles):
    with chdir(datafiles):
        b3lyp = dp.QCMethod('DFT', 'B3LYP')
        basis = dp.Basis(basis='pcseg-0', ri='def2-universal-JKFIT', admm='admm-1')
        water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
        points = np.load(f'{datafiles}/ep_points.npy')
        density = np.load(f'{datafiles}/lspy_h1density.npy')
        ep_ref = np.load(f'{datafiles}/ep.npy')
        ep = dp.lsdalton.electrostatic_potential(density, points, water, basis, b3lyp, ep_derivative_order=(0, 1))
    np.testing.assert_allclose(ep, ep_ref, atol=1e-12)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz',
    pytest.DATADIR / 'ep_points.npy',
    pytest.DATADIR / 'electronic_ep.npy',
    pytest.DATADIR / 'ep_mat.npy',
)
def test_pylsdalton_multipole_interaction_matrix(datafiles):
    with chdir(datafiles):
        b3lyp = dp.QCMethod('DFT', 'B3LYP')
        basis = dp.Basis(basis='pcseg-0', ri='def2-universal-JKFIT', admm='admm-1')
        water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
        points = np.load(f'{datafiles}/ep_points.npy')
        moments = np.load(f'{datafiles}/electronic_ep.npy')
        ep_mat_ref = np.load(f'{datafiles}/ep_mat.npy')
        ep_mat = dp.lsdalton.multipole_interaction_matrix(moments,
                                                          points,
                                                          water,
                                                          basis,
                                                          b3lyp,
                                                          multipole_orders=(0, 1))
    np.testing.assert_allclose(ep_mat, ep_mat_ref, atol=1e-12)


@pytest.mark.datafiles(
    pytest.DATADIR / 'He.xyz',
    pytest.DATADIR / 'ep_points.npy',
    pytest.DATADIR / 'ep_int.npy',
)
def test_pylsdalton_electrostatic_potential_integrals(datafiles):
    with chdir(datafiles):
        b3lyp = dp.QCMethod('DFT', 'B3LYP')
        basis = dp.Basis(basis='pcseg-0')
        he = dp.Molecule(input_file=(datafiles / 'He.xyz'))
        points = np.load(f'{datafiles}/ep_points.npy')
        ep_int_ref = np.load(f'{datafiles}/ep_int.npy')
        ep_int = dp.lsdalton.electrostatic_potential_integrals(points,
                                                               he,
                                                               basis,
                                                               b3lyp,
                                                               ep_derivative_order=(0, 1))
    np.testing.assert_allclose(ep_int, ep_int_ref, atol=1e-12)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz',
    pytest.DATADIR / 'ep_points.npy',
    pytest.DATADIR / 'electronic_ep.npy',
    pytest.DATADIR / 'lspy_h1density.npy',
)
def test_pylsdalton_exceptions(datafiles):
    with chdir(datafiles):
        b3lyp = dp.QCMethod('DFT', 'B3LYP')
        basis = dp.Basis(basis='pcseg-0', ri='def2-universal-JKFIT', admm='admm-1')
        water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
        points = np.load(f'{datafiles}/ep_points.npy')
        moments = np.load(f'{datafiles}/electronic_ep.npy')
        density = np.load(f'{datafiles}/lspy_h1density.npy')

        # nuclear_electrostatic_potential
        with pytest.raises(NotImplementedError,
                           match='Geometric derivatives are not implemented for nuclear electrostatic potentials.'):
            dp.lsdalton.nuclear_electrostatic_potential(points, water, geometric_derivative_order=1)
        with pytest.raises(NotImplementedError,
                           match='Magnetic derivatives are not implemented for nuclear electrostatic potentials.'):
            dp.lsdalton.nuclear_electrostatic_potential(points, water, magnetic_derivative_order=1)
        with pytest.raises(ValueError, match='Minimum EP-derivative order must be larger than or equal to zero'):
            dp.lsdalton.nuclear_electrostatic_potential(points, water, ep_derivative_order=(-1, 0))
        with pytest.raises(ValueError, match='Maximum EP-derivative order is limited to 1.'):
            dp.lsdalton.nuclear_electrostatic_potential(points, water, ep_derivative_order=(0, 2))
        with pytest.raises(
                ValueError,
                match='Maximum EP-derivative order must be larger than or equal to minimum EP-derivative order.'):
            dp.lsdalton.nuclear_electrostatic_potential(points, water, ep_derivative_order=(1, 0))

        # electrostatic_potential
        with pytest.raises(NotImplementedError,
                           match='Geometric derivatives are not implemented for electrostatic potentials.'):
            dp.lsdalton.electrostatic_potential(density, points, water, basis, b3lyp, geometric_derivative_order=1)
        with pytest.raises(NotImplementedError,
                           match='Magnetic derivatives are not implemented for electrostatic potentials.'):
            dp.lsdalton.electrostatic_potential(density, points, water, basis, b3lyp, magnetic_derivative_order=1)
        with pytest.raises(ValueError, match='Minimum EP-derivative order must be larger than or equal to zero'):
            dp.lsdalton.electrostatic_potential(density, points, water, basis, b3lyp, ep_derivative_order=(-1, 0))
        with pytest.raises(ValueError, match='Maximum EP-derivative order is limited to 1.'):
            dp.lsdalton.electrostatic_potential(density, points, water, basis, b3lyp, ep_derivative_order=(0, 2))
        with pytest.raises(
                ValueError,
                match='Maximum EP-derivative order must be larger than or equal to minimum EP-derivative order.'):
            dp.lsdalton.electrostatic_potential(density, points, water, basis, b3lyp, ep_derivative_order=(1, 0))

        # multipole_interaction_matrix
        with pytest.raises(
                NotImplementedError,
                match='Geometric derivatives higher than order 4 are not allowed for multipole interaction matrix.'):
            dp.lsdalton.multipole_interaction_matrix(moments,
                                                     points,
                                                     water,
                                                     basis,
                                                     b3lyp,
                                                     geometric_derivative_order=5)
        with pytest.raises(NotImplementedError,
                           match='Magnetic derivatives are not implemented for multipole interaction matrix.'):
            dp.lsdalton.multipole_interaction_matrix(moments,
                                                     points,
                                                     water,
                                                     basis,
                                                     b3lyp,
                                                     magnetic_derivative_order=1)
        with pytest.raises(ValueError, match='Minimum multipole order must be larger than or equal to zero.'):
            dp.lsdalton.multipole_interaction_matrix(moments,
                                                     points,
                                                     water,
                                                     basis,
                                                     b3lyp,
                                                     multipole_orders=(-1, 0))
        with pytest.raises(ValueError,
                           match='Maximum multipole order must be larger than or equal to minimum multipole order.'):
            dp.lsdalton.multipole_interaction_matrix(moments, points, water, basis, b3lyp, multipole_orders=(1, 0))

        # electrostatic_potential_integrals
        with pytest.raises(
                NotImplementedError,
                match='Geometric derivatives higher than order 4 are not allowed for electrostatic potential '
                'integrals.'):
            dp.lsdalton.electrostatic_potential_integrals(points,
                                                          water,
                                                          basis,
                                                          b3lyp,
                                                          geometric_derivative_order=5)
        with pytest.raises(NotImplementedError,
                           match='Magnetic derivatives are not implemented for electrostatic potential integrals.'):
            dp.lsdalton.electrostatic_potential_integrals(points, water, basis, b3lyp, magnetic_derivative_order=1)
        with pytest.raises(ValueError, match='Minimum EP-derivative order must be larger than or equal to zero.'):
            dp.lsdalton.electrostatic_potential_integrals(points, water, basis, b3lyp, ep_derivative_order=(-1, 0))
        with pytest.raises(
                ValueError,
                match='Maximum EP-derivative order must be larger than or equal to minimum EP-derivative order.'):
            dp.lsdalton.electrostatic_potential_integrals(points, water, basis, b3lyp, ep_derivative_order=(1, 0))

        # electronic_electrostatic_potential
        with pytest.raises(
                NotImplementedError,
                match='Geometric derivatives higher than order 4 are not allowed for electronic electrostatic '
                'potential.'):
            dp.lsdalton.electronic_electrostatic_potential(density,
                                                           points,
                                                           water,
                                                           basis,
                                                           b3lyp,
                                                           geometric_derivative_order=5)
        with pytest.raises(NotImplementedError,
                           match='Magnetic derivatives are not implemented for electronic electrostatic potential.'):
            dp.lsdalton.electronic_electrostatic_potential(density,
                                                           points,
                                                           water,
                                                           basis,
                                                           b3lyp,
                                                           magnetic_derivative_order=1)
        with pytest.raises(ValueError, match='Minimum EP-derivative order must be larger than or equal to zero.'):
            dp.lsdalton.electronic_electrostatic_potential(density,
                                                           points,
                                                           water,
                                                           basis,
                                                           b3lyp,
                                                           ep_derivative_order=(-1, 0))
        with pytest.raises(
                ValueError,
                match='Maximum EP-derivative order must be larger than or equal to minimum EP-derivative order.'):
            dp.lsdalton.electronic_electrostatic_potential(density,
                                                           points,
                                                           water,
                                                           basis,
                                                           b3lyp,
                                                           ep_derivative_order=(1, 0))
