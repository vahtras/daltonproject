import numpy as np
import pytest

from daltonproject import spectrum


def test_gaussian_normalization():
    freq = np.linspace(0.0, 10.0, 20000)
    gaussian = spectrum.gaussian(x=freq, x0=5.0, broadening_factor=0.6)
    np.testing.assert_allclose(np.trapz(y=gaussian, x=freq), 1.0)


def test_lorentzian_normalization():
    freq = np.linspace(0.0, 100.0, 200000)
    lorentzian = spectrum.lorentzian(x=freq, x0=50.0, broadening_factor=0.001)
    np.testing.assert_allclose(np.trapz(y=lorentzian, x=freq), 1.0, atol=2e-5)


def test_opa_convolution():
    excitation_energies = np.array([4.3587, 6.4307, 8.0472])
    oscillator_strengths = np.array([0.0002, 0.5035, 0.0061])
    max_abs = np.max(
        spectrum.convolute_one_photon_absorption(excitation_energies,
                                                 oscillator_strengths,
                                                 np.linspace(2, 15, 300),
                                                 broadening_factor=0.325))
    assert max_abs == pytest.approx(20900.925174314107)
    max_abs = np.max(
        spectrum.convolute_one_photon_absorption(excitation_energies,
                                                 oscillator_strengths,
                                                 np.linspace(7.4, 15, 300),
                                                 broadening_factor=0.325))
    assert max_abs == pytest.approx(253.20571306999807)


def test_tpa_convolution():
    excitation_energies = np.array([9.21080934, 12.0584537, 12.58553825, 13.41155621, 16.55956801])
    two_photon_strengths = np.array([1.18, 2.48, 6.39, 14.2, 14.9])
    ref_cross_sections = np.array([0.07456077, 0.29702821, 0.77662478, 1.88744448, 2.99770944])
    two_photon_cross_sections = spectrum.convolute_two_photon_absorption(excitation_energies, two_photon_strengths,
                                                                         excitation_energies / 2.0),
    np.testing.assert_allclose(two_photon_cross_sections[0], ref_cross_sections, atol=1e-5)


def test_vibrational_spectrum_convolution():
    frequencies = np.array([3774.187199, 3770.35119618, 1577.93730109, 1216.70365105, 1093.29384871, 168.67330713])
    ir_intensities = np.array(
        [1.71850995e+14, 6.15633785e+08, 2.29395927e+05, 1.27100673e+14, 8.18293119e+04, 3.84752149e+14])
    ref_spectrum = np.array([
        0.4337464845278196, 12.379136922806959, 0.2355355384966658, 0.07705961859477761, 0.054680841953687206,
        0.10463943705446163, 14.06919823699741, 0.12952310633224023, 0.034695234551524755, 0.01790866157584331,
        0.011991693949708044, 0.00936629046913227, 0.008251678320423485, 0.008159305200247668, 0.0091529025570663,
        0.011934705331784016, 0.018965933816206263, 0.041116015397002235, 0.1821148388042904, 8.107538241566763
    ])
    energy_range = np.arange(0.0, 4000.0, 200.0)
    ir_spectrum = spectrum.convolute_vibrational_spectrum(frequencies, ir_intensities, energy_range)
    np.testing.assert_allclose(ir_spectrum, ref_spectrum)
    raman_intensities = np.array(
        [6.78861941e-34, 1.89116350e-28, 7.86634131e-31, 1.22282130e-39, 1.03587278e-30, 7.50155206e-44])
    energy_range = np.arange(0.0, 4000.0, 200.0)
    ref_spectrum = np.array([
        4.614253830409389e-46, 5.271078351248021e-46, 6.170151456700657e-46, 7.611205974629776e-46,
        1.1076938780729507e-45, 4.646970063910785e-45, 3.982779564495169e-45, 2.2140404217309716e-45,
        5.194903536187268e-44, 2.1257465818332532e-45, 2.1028217775934955e-45, 2.5344803249794295e-45,
        3.264261747274744e-45, 4.4364278505503883e-45, 6.425737446360745e-45, 1.0172194249649605e-44,
        1.8534552059959297e-44, 4.3930002089998933e-44, 2.0752958471970736e-43, 6.78335013147104e-42
    ])
    raman_spectrum = spectrum.convolute_vibrational_spectrum(frequencies, raman_intensities, energy_range)
    np.testing.assert_allclose(raman_spectrum, ref_spectrum)
