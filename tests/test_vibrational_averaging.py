import numpy as np
import pytest

import daltonproject as dp
from daltonproject.utilities import chdir


@pytest.mark.datafiles(pytest.DATADIR / 'co.xyz')
def test_vibav_settings_init(datafiles):
    mol = dp.Molecule(input_file=datafiles / 'co.xyz')
    nmr_prop = dp.Property(nmr_shieldings=True)
    is_linear = True
    ref_hess = np.array(
        [[-2.37976708e-02, 0.00000000e+00, -2.38526101e-15, 2.37976708e-02, 0.00000000e+00, 2.38526101e-15],
         [0.00000000e+00, -2.37976708e-02, -1.50495827e-15, 0.00000000e+00, 2.37976708e-02, 1.50495827e-15],
         [-2.38526101e-15, -1.50495827e-15, 1.74116139e+00, 2.38526101e-15, 1.50495827e-15, -1.74116139e+00],
         [2.37976708e-02, 0.00000000e+00, 2.38526101e-15, -2.37976708e-02, 0.00000000e+00, -2.38526101e-15],
         [0.00000000e+00, 2.37976708e-02, 1.50495827e-15, 0.00000000e+00, -2.37976708e-02, -1.50495827e-15],
         [2.38526101e-15, 1.50495827e-15, -1.74116139e+00, -2.38526101e-15, -1.50495827e-15, 1.74116139e+00]])
    with chdir(datafiles):
        va_settings = dp.VibAvSettings(molecule=mol,
                                       property_program='dalton',
                                       is_mol_linear=is_linear,
                                       hessian=ref_hess,
                                       property_obj=nmr_prop,
                                       stepsize=0.05,
                                       differentiation_method='polynomial',
                                       temperature=298,
                                       polynomial_fitting_order=3,
                                       plot_polyfittings=True)
    assert isinstance(va_settings.molecule, dp.Molecule)
    assert isinstance(va_settings.property_program, str)
    assert isinstance(va_settings.is_mol_linear, bool)
    np.testing.assert_allclose(va_settings.is_mol_linear, is_linear)
    assert va_settings.molecule == mol
    assert isinstance(va_settings.hessian, np.ndarray)
    np.testing.assert_allclose(va_settings.hessian, ref_hess, atol=1e-5)
    assert isinstance(va_settings.property_obj, dp.Property)
    assert isinstance(va_settings.stepsize, float)
    assert va_settings.stepsize == 0.05
    assert isinstance(va_settings.differentiation_method, str)
    assert va_settings.differentiation_method == 'polynomial'
    assert isinstance(va_settings.temperature, int)
    assert va_settings.temperature == 298
    assert isinstance(va_settings.polynomial_fitting_order, int)
    assert va_settings.polynomial_fitting_order == 3
    assert isinstance(va_settings.plot_polyfittings, bool)
    assert va_settings.plot_polyfittings is True
    assert 'nmr_shieldings' in nmr_prop.settings
    nmr_prop = dp.Property(polarizabilities=True)
    with chdir(datafiles):
        va_settings = dp.VibAvSettings(molecule=mol,
                                       property_program='dalton',
                                       is_mol_linear=is_linear,
                                       hessian=ref_hess,
                                       property_obj=nmr_prop,
                                       stepsize=5,
                                       differentiation_method='linear',
                                       temperature=10.5,
                                       linear_point_stencil=3)
    assert isinstance(va_settings.stepsize, int)
    assert va_settings.stepsize == 5
    assert isinstance(va_settings.differentiation_method, str)
    assert va_settings.differentiation_method == 'linear'
    assert isinstance(va_settings.temperature, float)
    assert va_settings.temperature == 10.5
    assert isinstance(va_settings.linear_point_stencil, int)
    assert va_settings.linear_point_stencil == 3
    assert 'polarizabilities' in nmr_prop.settings
    nmr_prop = dp.Property(spin_spin_couplings=True)
    with chdir(datafiles):
        va_settings = dp.VibAvSettings(molecule=mol,
                                       property_program='dalton',
                                       is_mol_linear=is_linear,
                                       hessian=ref_hess,
                                       property_obj=nmr_prop,
                                       stepsize=0.05,
                                       differentiation_method='linear',
                                       temperature=10.5,
                                       linear_point_stencil=5)
    assert va_settings.linear_point_stencil == 5
    assert 'spin_spin_couplings' in nmr_prop.settings


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz')
def test_vibav_settings_init_exceptions(datafiles):
    with pytest.raises(TypeError):
        dp.VibAvSettings()
    hessian_ref = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    with pytest.raises(TypeError, match='Molecule must be a Molecule class object'):
        dp.VibAvSettings(molecule=dp.Property(spin_spin_couplings=True),
                         property_program='dalton',
                         is_mol_linear=False,
                         hessian=hessian_ref,
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=0.05,
                         differentiation_method='polynomial',
                         temperature=298,
                         polynomial_fitting_order=3)
    with pytest.raises(TypeError, match='is_mol_linear must be a boolean.'):
        dp.VibAvSettings(molecule=dp.Molecule(input_file=datafiles / 'water.xyz'),
                         property_program='dalton',
                         is_mol_linear=['False'],
                         hessian=hessian_ref,
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=0.05,
                         differentiation_method='polynomial',
                         temperature=298,
                         polynomial_fitting_order=3)
    with pytest.raises(TypeError, match='Hessian must be a numpy array.'):
        dp.VibAvSettings(molecule=dp.Molecule(input_file=datafiles / 'water.xyz'),
                         property_program='dalton',
                         is_mol_linear=False,
                         hessian=['0.1'],
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=0.05,
                         differentiation_method='polynomial',
                         temperature=298,
                         polynomial_fitting_order=3)
    with pytest.raises(TypeError, match='Stepsize must be a float/integer.'):
        dp.VibAvSettings(molecule=dp.Molecule(input_file=datafiles / 'water.xyz'),
                         property_program='dalton',
                         is_mol_linear=False,
                         hessian=hessian_ref,
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=['0.05'],
                         differentiation_method='polynomial',
                         temperature=298,
                         polynomial_fitting_order=3)
    with pytest.raises(TypeError, match='differentiation_method must be "linear" or "polynomial", got 0.05'):
        dp.VibAvSettings(molecule=dp.Molecule(input_file=datafiles / 'water.xyz'),
                         property_program='dalton',
                         is_mol_linear=False,
                         hessian=hessian_ref,
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=0.05,
                         differentiation_method='0.05',
                         temperature=298,
                         polynomial_fitting_order=3)
    with pytest.raises(TypeError,
                       match='Allowed to only specify linear_point_stencil or polynomial_fitting_order.'):
        dp.VibAvSettings(molecule=dp.Molecule(input_file=datafiles / 'water.xyz'),
                         property_program='dalton',
                         is_mol_linear=False,
                         hessian=hessian_ref,
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=0.05,
                         differentiation_method='polynomial',
                         temperature=298,
                         polynomial_fitting_order=3,
                         linear_point_stencil=3)
    with pytest.raises(TypeError, match='Must specify linear stencil number.'):
        dp.VibAvSettings(molecule=dp.Molecule(input_file=datafiles / 'water.xyz'),
                         property_program='dalton',
                         is_mol_linear=False,
                         hessian=hessian_ref,
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=0.05,
                         differentiation_method='linear',
                         temperature=298)
    with pytest.raises(TypeError, match='Must specify polynomial fitting order.'):
        dp.VibAvSettings(molecule=dp.Molecule(input_file=datafiles / 'water.xyz'),
                         property_program='dalton',
                         is_mol_linear=False,
                         hessian=hessian_ref,
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=0.05,
                         differentiation_method='polynomial',
                         temperature=298)
    with pytest.raises(TypeError, match='polynomial_fitting_order must be an integer.'):
        dp.VibAvSettings(molecule=dp.Molecule(input_file=datafiles / 'water.xyz'),
                         property_program='dalton',
                         is_mol_linear=False,
                         hessian=hessian_ref,
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=0.05,
                         differentiation_method='polynomial',
                         temperature=298,
                         polynomial_fitting_order=['3'])
    with pytest.raises(TypeError, match='linear_point_stencil must be an integer.'):
        dp.VibAvSettings(molecule=dp.Molecule(input_file=datafiles / 'water.xyz'),
                         property_program='dalton',
                         is_mol_linear=False,
                         hessian=hessian_ref,
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=0.05,
                         differentiation_method='linear',
                         temperature=298,
                         linear_point_stencil=['3'])
    with pytest.raises(TypeError, match='Linear fitting order must be 3 or 5.'):
        dp.VibAvSettings(molecule=dp.Molecule(input_file=datafiles / 'water.xyz'),
                         property_program='dalton',
                         is_mol_linear=False,
                         hessian=hessian_ref,
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=0.05,
                         differentiation_method='linear',
                         temperature=298,
                         linear_point_stencil=7)
    with pytest.raises(TypeError, match='temperature must be a float or integer.'):
        dp.VibAvSettings(molecule=dp.Molecule(input_file=datafiles / 'water.xyz'),
                         property_program='dalton',
                         is_mol_linear=False,
                         hessian=hessian_ref,
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=0.05,
                         differentiation_method='linear',
                         temperature=['298'],
                         linear_point_stencil=3)
    with pytest.raises(TypeError, match='plot_polyfittings must be a boolean.'):
        dp.VibAvSettings(molecule=dp.Molecule(input_file=datafiles / 'water.xyz'),
                         property_program='dalton',
                         is_mol_linear=False,
                         hessian=hessian_ref,
                         property_obj=dp.Property(spin_spin_couplings=True),
                         stepsize=0.05,
                         differentiation_method='linear',
                         temperature=298,
                         linear_point_stencil=3,
                         plot_polyfittings=['True'])


@pytest.mark.datafiles(pytest.DATADIR / 'co.xyz')
def test_vibav_correct_displacement(datafiles):
    molecule = dp.Molecule(input_file=(datafiles / 'co.xyz'))
    vib_prop = dp.Property(nmr_shieldings=True)
    ref_hessian = np.array(
        [[-2.37976708e-02, 0.00000000e+00, -2.38526101e-15, 2.37976708e-02, 0.00000000e+00, 2.38526101e-15],
         [0.00000000e+00, -2.37976708e-02, -1.50495827e-15, 0.00000000e+00, 2.37976708e-02, 1.50495827e-15],
         [-2.38526101e-15, -1.50495827e-15, 1.74116139e+00, 2.38526101e-15, 1.50495827e-15, -1.74116139e+00],
         [2.37976708e-02, 0.00000000e+00, 2.38526101e-15, -2.37976708e-02, 0.00000000e+00, -2.38526101e-15],
         [0.00000000e+00, 2.37976708e-02, 1.50495827e-15, 0.00000000e+00, -2.37976708e-02, -1.50495827e-15],
         [2.38526101e-15, 1.50495827e-15, -1.74116139e+00, -2.38526101e-15, -1.50495827e-15, 1.74116139e+00]])

    with chdir(datafiles):
        va_settings = dp.VibAvSettings(molecule=molecule,
                                       property_program='dalton',
                                       is_mol_linear=True,
                                       hessian=ref_hessian,
                                       property_obj=vib_prop,
                                       stepsize=0.05,
                                       differentiation_method='linear',
                                       temperature=298,
                                       linear_point_stencil=3)
        molecules = []
        for i in va_settings.file_list:
            molecules.append(dp.Molecule(input_file=i))
    displace_geom1 = np.array([[0, 0, 0.6385809], [-0, 0, -0.4884958]])
    displace_geom2 = np.array([[-0, 0, 0.6410702], [0, 0, -0.4903634]])
    np.testing.assert_allclose(molecules[0].coordinates, displace_geom1, atol=1e-5)
    np.testing.assert_allclose(molecules[1].coordinates, displace_geom2, atol=1e-5)


@pytest.mark.datafiles(pytest.DATADIR / 'co.xyz', pytest.DATADIR / 'co_hess_mode_0_minus1.tar.gz',
                       pytest.DATADIR / 'co_hess_mode_0_plus1.tar.gz',
                       pytest.DATADIR / 'co_hess_standard_property.tar.gz',
                       pytest.DATADIR / 'co_shieldings_mode_0_minus1.tar.gz',
                       pytest.DATADIR / 'co_shieldings_mode_0_plus1.tar.gz',
                       pytest.DATADIR / 'co_shieldings_standard_property.tar.gz')
def test_vibav_correct_output_shieldings(datafiles):
    atom_isotopes = ['C13', 'O17']
    molecule = dp.Molecule(input_file=(datafiles / 'co.xyz'), isotopes=atom_isotopes)
    ref_hessian = dp.dalton.OutputParser(datafiles / 'co_hess_standard_property')
    vib_prop = dp.Property(nmr_shieldings=True)
    with chdir(datafiles):
        va_settings = dp.VibAvSettings(molecule=molecule,
                                       property_program='dalton',
                                       is_mol_linear=True,
                                       hessian=ref_hessian.hessian,
                                       property_obj=vib_prop,
                                       stepsize=0.05,
                                       differentiation_method='linear',
                                       temperature=0,
                                       linear_point_stencil=3)
        dalton_results_hess = []
        dalton_results_nmr = []
        for i in va_settings.file_list:
            file = i.split('.')[0]
            dalton_results_hess.append(dp.dalton.OutputParser(datafiles/'co_hess_' + file))
            dalton_results_nmr.append(dp.dalton.OutputParser(datafiles/'co_shieldings_' + file))
        result = dp.ComputeVibAvCorrection(dalton_results_hess, dalton_results_nmr, va_settings)
    cfc_ref = np.array([[-661.93703572]])
    property_first_derivative_ref = np.array([[-28.52026795, -42.17585009]])
    property_second_derivative_ref = np.array([[-4.31454716, -8.6730811]])
    mean_displacement_q_ref = np.array([0.06622094])
    mean_displacement_q2_ref = np.array([0.5])
    vibrational_correction_ref = np.array([-2.96727578, -4.96119477])
    np.testing.assert_allclose(result.cubic_force_constants, cfc_ref, atol=1e-6)
    np.testing.assert_allclose(result.property_first_derivatives, property_first_derivative_ref, atol=1e-6)
    np.testing.assert_allclose(result.property_second_derivatives, property_second_derivative_ref, atol=1e-6)
    np.testing.assert_allclose(result.mean_displacement_q, mean_displacement_q_ref, atol=1e-6)
    np.testing.assert_allclose(result.mean_displacement_q2, mean_displacement_q2_ref, atol=1e-6)
    np.testing.assert_allclose(result.vibrational_corrections, vibrational_correction_ref, atol=1e-6)


@pytest.mark.datafiles(pytest.DATADIR / 'co.xyz', pytest.DATADIR / 'co_hess_mode_0_minus1.tar.gz',
                       pytest.DATADIR / 'co_hess_mode_0_plus1.tar.gz',
                       pytest.DATADIR / 'co_hess_standard_property.tar.gz',
                       pytest.DATADIR / 'co_sscc_mode_0_minus1.out', pytest.DATADIR / 'co_sscc_mode_0_plus1.out',
                       pytest.DATADIR / 'co_sscc_standard_property.out')
def test_vibav_correct_output_spin_spin(datafiles):
    atom_isotopes = ['C13', 'O17']
    molecule = dp.Molecule(input_file=(datafiles / 'co.xyz'), isotopes=atom_isotopes)
    ref_hessian = dp.dalton.OutputParser(datafiles / 'co_hess_standard_property')
    vib_prop = dp.Property(spin_spin_couplings=True)
    with chdir(datafiles):
        va_settings = dp.VibAvSettings(molecule=molecule,
                                       property_program='dalton',
                                       is_mol_linear=True,
                                       hessian=ref_hessian.hessian,
                                       property_obj=vib_prop,
                                       stepsize=0.05,
                                       differentiation_method='linear',
                                       temperature=0,
                                       linear_point_stencil=3)
        dalton_results_hess = []
        dalton_results_nmr = []
        for i in va_settings.file_list:
            file = i.split('.')[0]
            dalton_results_hess.append(dp.dalton.OutputParser(datafiles/'co_hess_' + file))
            dalton_results_nmr.append(dp.dalton.OutputParser(datafiles/'co_sscc_' + file))
        result = dp.ComputeVibAvCorrection(dalton_results_hess, dalton_results_nmr, va_settings)
    cfc_ref = np.array([[-661.93703572]])
    property_first_derivative_ref = np.array([[-0.606]])
    property_second_derivative_ref = np.array([[-2.8]])
    mean_displacement_q_ref = np.array([0.06622094])
    mean_displacement_q2_ref = np.array([0.5])
    vibrational_correction_ref = np.array([-0.74012989])
    np.testing.assert_allclose(result.cubic_force_constants, cfc_ref, atol=1e-6)
    np.testing.assert_allclose(result.property_first_derivatives, property_first_derivative_ref, atol=1e-6)
    np.testing.assert_allclose(result.property_second_derivatives, property_second_derivative_ref, atol=1e-6)
    np.testing.assert_allclose(result.mean_displacement_q, mean_displacement_q_ref, atol=1e-6)
    np.testing.assert_allclose(result.mean_displacement_q2, mean_displacement_q2_ref, atol=1e-6)
    np.testing.assert_allclose(result.vibrational_corrections, vibrational_correction_ref, atol=1e-6)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz', pytest.DATADIR / 'vibav_water_hess_standard_property.fchk',
    pytest.DATADIR / 'vibav_water_prop_standard_property.fchk',
    pytest.DATADIR / 'vibav_water_hess_mode_0_minus1.fchk', pytest.DATADIR / 'vibav_water_hess_mode_0_plus1.fchk',
    pytest.DATADIR / 'vibav_water_prop_mode_0_minus1.fchk', pytest.DATADIR / 'vibav_water_prop_mode_0_plus1.fchk',
    pytest.DATADIR / 'vibav_water_hess_mode_1_minus1.fchk', pytest.DATADIR / 'vibav_water_hess_mode_1_plus1.fchk',
    pytest.DATADIR / 'vibav_water_prop_mode_1_minus1.fchk', pytest.DATADIR / 'vibav_water_prop_mode_1_plus1.fchk',
    pytest.DATADIR / 'vibav_water_hess_mode_2_minus1.fchk', pytest.DATADIR / 'vibav_water_hess_mode_2_plus1.fchk',
    pytest.DATADIR / 'vibav_water_prop_mode_2_minus1.fchk', pytest.DATADIR / 'vibav_water_prop_mode_2_plus1.fchk')
def test_rotational_contribution(datafiles):
    atom_isotopes = ['O17', 'H1', 'H1']
    molecule = dp.Molecule(input_file=(datafiles / 'water.xyz'), isotopes=atom_isotopes)
    prop_output = dp.gaussian.OutputParser(datafiles / 'vibav_water_hess_standard_property')
    vib_prop = dp.Property(optical_rotations={'frequencies': [0.1, 0.2, 0.3]})
    with chdir(datafiles):
        va_settings = dp.VibAvSettings(molecule=molecule,
                                       property_program='gaussian',
                                       is_mol_linear=False,
                                       hessian=prop_output.hessian,
                                       property_obj=vib_prop,
                                       stepsize=0.05,
                                       differentiation_method='linear',
                                       temperature=298,
                                       linear_point_stencil=3,
                                       plot_polyfittings=True)
        gaussian_results_hess = []
        gaussian_results_nmr = []
        for i in va_settings.file_list:
            file = i.split('.')[0]
            gaussian_results_hess.append(dp.gaussian.OutputParser(datafiles/'vibav_water_hess_' + file))
            gaussian_results_nmr.append(dp.gaussian.OutputParser(datafiles/'vibav_water_prop_' + file))
        rot_contribution = dp.ComputeVibAvCorrection(gaussian_results_hess, gaussian_results_nmr,
                                                     va_settings).rotational_contribution()
    ref = [2.84322133e-01, 1.65624587e-15, 5.31907319e+00]
    np.testing.assert_allclose(rot_contribution, ref, atol=1e-6)
