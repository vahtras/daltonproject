import textwrap

import numpy as np
import pytest

import daltonproject as dp
from daltonproject.utilities import chdir


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz', )
def test_dalton_tpa(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='pcseg-0')
    energy = dp.Property(energy=True, two_photon_absorption=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with chdir(datafiles):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result_1 = dp.dalton.compute(molecule=water,
                                     basis=basis,
                                     qc_method=hf,
                                     properties=energy,
                                     compute_settings=settings)
    np.testing.assert_allclose(result_1.energy, -75.702096519278)
    np.testing.assert_allclose(result_1.excitation_energies,
                               [9.21080934, 12.0584537, 12.58553825, 13.41155621, 16.55956801])
    np.testing.assert_allclose(result_1.two_photon_strengths, [1.18, 2.48, 6.39, 14.2, 14.9])
    np.testing.assert_allclose(result_1.two_photon_cross_sections, [0.0732, 0.264, 0.741, 1.87, 2.99])
    elements_s1 = np.array([[-0.0, 2.1, 0.0], [2.1, -0.0, -0.0], [0.0, -0.0, 0.0]])
    elements_s2 = np.array([[0.0, 0.0, 0.0], [0.0, 0.0, -3.1], [0.0, -3.1, -0.0]])
    elements_s3 = np.array([[0.0, 0.0, 4.9], [0.0, 0.0, -0.0], [4.9, -0.0, 0.0]])
    elements_s4 = np.array([[-6.9, 0.0, 0.0], [0.0, -1.1, -0.0], [0.0, -0.0, -2.3]])
    elements_s5 = np.array([[0.0, 0.0, -7.5], [0.0, 0.0, 0.0], [-7.5, 0.0, 0.0]])
    assert np.allclose(result_1.two_photon_tensors[0], elements_s1) \
        or np.allclose(result_1.two_photon_tensors[0], -elements_s1)
    assert np.allclose(result_1.two_photon_tensors[1], elements_s2) \
        or np.allclose(result_1.two_photon_tensors[1], -elements_s2)
    assert np.allclose(result_1.two_photon_tensors[2], elements_s3) \
        or np.allclose(result_1.two_photon_tensors[2], -elements_s3)
    assert np.allclose(result_1.two_photon_tensors[3], elements_s4) \
        or np.allclose(result_1.two_photon_tensors[3], -elements_s4)
    assert np.allclose(result_1.two_photon_tensors[4], elements_s5) \
        or np.allclose(result_1.two_photon_tensors[4], -elements_s5)
    with pytest.raises(ValueError, match='One-photon oscillator strengths not available from TPA calculation.'):
        result_1.oscillator_strengths
    excita = dp.Property(excitation_energies=True)
    with chdir(datafiles):
        result_2 = dp.dalton.compute(molecule=water,
                                     basis=basis,
                                     qc_method=hf,
                                     properties=excita,
                                     compute_settings=settings)
    np.testing.assert_allclose(result_1.excitation_energies, result_2.excitation_energies, atol=1e-5)


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz', )
def test_dalton_dipole(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='pcseg-0')
    prop = dp.Property(dipole=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with chdir(datafiles):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result = dp.dalton.compute(molecule=water,
                                   basis=basis,
                                   qc_method=hf,
                                   properties=prop,
                                   compute_settings=settings)
    np.testing.assert_allclose(result.dipole, [1.17001531, 0., 0.])


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz', )
def test_dalton_polarizability(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='pcseg-0')
    prop = dp.Property(polarizabilities=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with chdir(datafiles):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result = dp.dalton.compute(molecule=water,
                                   basis=basis,
                                   qc_method=hf,
                                   properties=prop,
                                   compute_settings=settings)
    ref_alpha = np.array([[[5.48684006e+00, 0., 0.], [0., 1.47177553e+00, 0.], [0., 0., 4.40273540e+00]]])
    ref_freq = np.array([0.000])
    np.testing.assert_allclose(result.polarizabilities.values, ref_alpha, atol=1e-8)
    np.testing.assert_allclose(result.polarizabilities.frequencies, ref_freq, atol=1e-8)


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz', )
def test_dalton_first_hyperpolarizability(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='pcseg-0')
    prop = dp.Property(first_hyperpolarizability=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with chdir(datafiles):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result = dp.dalton.compute(molecule=water,
                                   basis=basis,
                                   qc_method=hf,
                                   properties=prop,
                                   compute_settings=settings)
    ref_beta = np.array([[[0., 0., 0.], [-0., -1.23851149, -0.], [0., -0., -18.90903982]],
                         [[0., -1.2385114, -0.], [-1.2385114, -0., 0.], [-0., 0., -0.]],
                         [[0., -0., -18.90903981], [-0., 0., -0.], [-18.90903981, -0., 0.]]])
    np.testing.assert_allclose(result.first_hyperpolarizability, ref_beta)


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz', )
def test_dalton_nmr_shieldings(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='pcseg-0')
    prop = dp.Property(nmr_shieldings=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with chdir(datafiles):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result = dp.dalton.compute(molecule=water,
                                   basis=basis,
                                   qc_method=hf,
                                   properties=prop,
                                   compute_settings=settings)
    ref_nmr_shieldings = [401.10078, 37.074151, 37.074151]
    np.testing.assert_allclose(result.nmr_shieldings, ref_nmr_shieldings)


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz', )
def test_dalton_nmr_spin_spin_couplings(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='pcseg-0')
    prop = dp.Property(spin_spin_couplings=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with chdir(datafiles):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result = dp.dalton.compute(molecule=water,
                                   basis=basis,
                                   qc_method=hf,
                                   properties=prop,
                                   compute_settings=settings)
    ref_spin_spin_couplings = [[4.8974], [-1.8712], [1.8942], [-10.9994]]
    ref_spin_spin_labels = ['H1 H1']
    np.testing.assert_allclose(result.spin_spin_couplings, ref_spin_spin_couplings)
    assert result.spin_spin_labels == ref_spin_spin_labels


def test_dalton_hfc(tmpdir):
    dft = dp.QCMethod('DFT', 'BP86')
    mol = dp.Molecule(atoms=textwrap.dedent("""Ti    0.000000  0.000000   0.000000
                                               F     1.756834  0.000000   0.000000
                                               F    -0.878417  1.521463   0.000000
                                               F    -0.878417 -1.521463   0.000000"""))
    basis = dp.Basis(basis='STO-3G')
    prop = dp.Property(energy=True, hyperfine_couplings={'atoms': [1, 2]})
    with chdir(tmpdir):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result = dp.dalton.compute(molecule=mol,
                                   basis=basis,
                                   qc_method=dft,
                                   properties=prop,
                                   compute_settings=settings)
    np.testing.assert_allclose(result.energy, -1136.250771376731)
    np.testing.assert_allclose(result.hyperfine_couplings.polarization, [0.395462, -0.006226], atol=1e-5)
    np.testing.assert_allclose(result.hyperfine_couplings.direct, [14.941623, 0.318802], atol=1e-5)
    np.testing.assert_allclose(result.hyperfine_couplings.total, [15.337085, 0.312577], atol=1e-5)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz',
    pytest.DATADIR / 'Ammonia.xyz',
    pytest.DATADIR / 'Acetylene.xyz',
    pytest.DATADIR / 'ethanol.xyz',
)
def test_dalton_compute_farm(datafiles):
    hf = dp.QCMethod(qc_method='HF')
    pcseg0 = dp.Basis(basis='pcseg-0')
    energy = dp.Property(energy=True)
    molecules = []
    for molecule in ['water', 'Ammonia', 'Acetylene', 'ethanol']:
        molecules.append(dp.Molecule(input_file=(datafiles / f'{molecule}.xyz')))
    with chdir(datafiles):
        settings = dp.ComputeSettings(jobs_per_node=2)
        results = dp.dalton.compute_farm(molecules=molecules,
                                         basis=pcseg0,
                                         qc_method=hf,
                                         properties=energy,
                                         compute_settings=settings)
    ref_energies = [-75.702096519278, -56.014788641629, -76.616128280751, -153.616385456613]
    for result, ref_energy in zip(results, ref_energies):
        assert result.energy == pytest.approx(ref_energy)


@pytest.mark.datafiles(pytest.DATADIR / 'Xenontetrafluoride.xyz', )
def test_ecp_works(datafiles, tmpdir):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='def2-SVP')
    prop = dp.Property(energy=True)
    mol = dp.Molecule(input_file=(datafiles / 'Xenontetrafluoride.xyz'))
    with chdir(tmpdir):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result = dp.dalton.compute(molecule=mol,
                                   basis=basis,
                                   qc_method=hf,
                                   properties=prop,
                                   compute_settings=settings)
    np.testing.assert_allclose(result.energy, -725.320295554799)
    np.testing.assert_allclose(result.num_electrons, 62)


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz', )
def vibav_freq_dependent_polarizabilites(datafiles, tmpdir):
    atom_isotopes = ['O17', 'H1', 'H1']
    hf = dp.QCMethod(qc_method='HF', scf_threshold=1e-10)
    basis = dp.Basis(basis='STO-3G')
    molecule = dp.Molecule(datafiles / 'water.xyz', isotopes=atom_isotopes)
    hess = dp.Property(hessian=True)
    prop_output = dp.dalton.compute(molecule, basis, hf)
    vib_prop = dp.Property(polarizabilities={'frequencies': [0.1, 0.2, 0.3]})
    va_settings = dp.VibAvSettings(molecule=molecule,
                                   property_program='dalton',
                                   is_mol_linear=False,
                                   hessian=prop_output.hessian,
                                   property_obj=vib_prop,
                                   stepsize=0.05,
                                   differentiation_method='linear',
                                   temperature=0,
                                   linear_point_stencil=3,
                                   plot_polyfittings=True)
    with chdir(tmpdir):
        molecules = []
        for i in va_settings.file_list:
            molecules.append(dp.Molecule(input_file=i, isotopes=atom_isotopes))
        dalton_results_hess = dp.dalton.compute_farm(molecules, basis, hf, hess)
        dalton_results_nmr = dp.dalton.compute_farm(molecules, basis, hf, vib_prop)
        result = dp.ComputeVibAvCorrection(dalton_results_hess, dalton_results_nmr, va_settings)
    cubic_force_constants = [[6.42299246e+01, 9.99301049e+02, 2.25089840e+02],
                             [2.56702456e-07, 6.13284805e-10, 2.21139500e-09],
                             [-3.51513350e+02, -1.51487376e+03, -1.66325666e+03]]
    prop_first_derivatives = [[-0.1267473, 0., 0.31500611], [-0.13024122, 0., 0.32328528],
                              [-0.14222028, 0., 0.35055492], [-0.16913282, 0., 0.40631479]]
    prop_second_derivatives = [[0.01068445, 0.04119414, 0.03710042], [0.01161126, 0.04368283, 0.03943051],
                               [0.01542182, 0.05217387, 0.04753796], [0.02751763, 0.07081065, 0.06630191]]
    mean_displacement = [-1.34416282e-01, -1.25586352e-11, 1.59478885e-01]
    mean_squared_displacement = [0.5, 0.5, 0.5]
    vibav_corrections = [0.08951848, 0.09274487, 0.10380624, 0.12869038]
    np.testing.assert_allclose(result.cubic_force_constants, cubic_force_constants)
    np.testing.assert_allclose(result.property_first_derivatives, prop_first_derivatives)
    np.testing.assert_allclose(result.property_second_derivatives, prop_second_derivatives)
    np.testing.assert_allclose(result.mean_displacement_q, mean_displacement)
    np.testing.assert_allclose(result.mean_displacement_q2, mean_squared_displacement)
    np.testing.assert_allclose(result.vibrational_corrections, vibav_corrections)
