import numpy as np

import daltonproject as dp


def test_numerical_derivatives_linear_cfcs():
    phi = np.array([[[0.12740145]], [[0.12216584]]])
    dq = np.array([0.012875496897647245])
    order = 3
    k_ijk = dp.linear_cfcs(phi, dq, order)
    k_ijk_ref = np.array([[[-0.20331673]]])
    np.testing.assert_allclose(k_ijk, k_ijk_ref, atol=1e-7)


def test_numerical_derivatives_polynomial_cfcs():
    phi = np.array([[[0.13010242]], [[0.12740145]], [[0.12216584]], [[0.11962804]]])
    phi_eq = np.array([[0.12475671]]).reshape(1, 1)
    dq = np.array([0.012875496897647245])
    order = 3
    k_ijk = dp.polynomial_cfcs(phi, phi_eq, dq, order)
    k_ijk_ref = np.array([[[-0.20329626]]])
    np.testing.assert_allclose(k_ijk, k_ijk_ref, atol=1e-5)


def test_numerical_linear_property_derivatives():
    motherlist = np.array([[208.8884, 79.0971, 214.6691, 81.2861]])
    e_list = np.array([211.7336, 80.1745])
    h = 0.05
    points = 3
    first_derivatives_ref = np.array([57.807, 21.89])
    second_derivatives_ref = np.array([36.12, 13.68])
    first_derivatives, second_derivatives = dp.linear_property(motherlist, e_list, h, points)
    np.testing.assert_allclose(first_derivatives_ref, first_derivatives, atol=1e-7)
    np.testing.assert_allclose(second_derivatives_ref, second_derivatives, atol=1e-7)


def test_numerical_polynomial_property_derivatives():
    motherlist = np.array([[206.1109, 78.0454, 208.8884, 79.0971, 214.6691, 81.2861, 217.6909, 82.4303]])
    e_list = np.array([211.7336, 80.1745])
    h = 0.05
    order = 3
    max_degree_derivative = 2
    dcoefs_mat_ref = np.array([[[57.776, 33.73142857], [33.73142857, 74.4], [37.2, 0.]],
                               [[21.8785, 12.77306122], [12.77306122, 27.6], [13.8, 0.]]])
    dcoefs_mat = dp.polynomial_property(motherlist, e_list, h, order, max_degree_derivative, plot=False)
    np.testing.assert_allclose(dcoefs_mat, dcoefs_mat_ref, atol=1e-7)
