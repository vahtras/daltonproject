import matplotlib.pyplot as plt

import daltonproject as dp

hf = dp.QCMethod(qc_method='HF')
basis = dp.Basis(basis='pcseg-0')
h2o2 = dp.Molecule(input_file='data/H2O2.xyz')

geo_opt = dp.Property(geometry_optimization=True)

# First we optimize the geometry
opt_output = dp.lsdalton.compute(h2o2, basis, hf, geo_opt)
h2o2.coordinates = opt_output.final_geometry

# Then we compute the Hessian as well as dipole and polarizability gradients
props = dp.Property(hessian=True, dipole_gradients=True, polarizability_gradients={'frequencies': [0.07198]})
prop_output = dp.lsdalton.compute(h2o2, basis, hf, props)

# Perform a vibrational analysis
vib_ana = dp.vibrational_analysis(molecule=h2o2,
                                  hessian=prop_output.hessian,
                                  dipole_gradients=prop_output.dipole_gradients,
                                  polarizability_gradients=prop_output.polarizability_gradients)

# We can plot the IR and Raman spectra in two separate figures
ir_axes = dp.spectrum.plot_ir_spectrum(vib_ana)
plt.show()
raman_axes = dp.spectrum.plot_raman_spectrum(vib_ana)
plt.show()
# Alternatively, they can be in the same plot with dual y-axes
fig_dual_axis, ax1 = plt.subplots()
ax1 = dp.spectrum.plot_ir_spectrum(vib_ana, ax=ax1, color='blue', label='IR')
ax2 = ax1.twinx()
ax2 = dp.spectrum.plot_raman_spectrum(vib_ana, ax=ax2, color='red', label='Raman')
fig_dual_axis.legend(loc='center')
plt.show()
# Or separately but in the same figure
fig_multi_row, (ax1, ax2) = plt.subplots(nrows=2)
dp.spectrum.plot_ir_spectrum(vib_ana, ax=ax1)
dp.spectrum.plot_raman_spectrum(vib_ana, ax=ax2)
plt.show()
