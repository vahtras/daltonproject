import daltonproject as dp

hf = dp.QCMethod(qc_method='HF')
loprop_basis = dp.Basis('loprop-6-31G*')
pcseg = dp.Basis('pcseg-0')
exci = dp.Property(excitation_energies=True)
settings = dp.ComputeSettings(mpi_num_procs=6, jobs_per_node=1)

pe = dp.pyframe_ifc.PolarizableEmbedding('./data/4-nitrophenolate_in_water.pdb')
pnp = pe.extract_core_region(fragment_name='4NP', fragment_id=1, distance=2.2)
pe.define_environment(distance=2.6, core_region=pnp)
pe.compute_potential_parameters(qc_method=hf, basis=loprop_basis, compute_settings=settings)

output = dp.dalton.compute(pnp, pcseg, hf, exci, pe)
print(output.excitation_energies)

pde = dp.pyframe_ifc.PolarizableDensityEmbedding('./data/4-nitrophenolate_in_water.pdb')
pnp = pde.extract_core_region(fragment_name='4NP', fragment_id=1, distance=2.2)
pde.define_environment(distance=2.6, core_region=pnp)
pde.compute_potential_parameters(pnp, pcseg, qc_method=hf, basis=loprop_basis, compute_settings=settings)

output = dp.dalton.compute(pnp, pcseg, hf, exci, pde)
print(output.excitation_energies)
