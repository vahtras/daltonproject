import math

import numpy as np

import daltonproject as dp

hf = dp.QCMethod('HF')
basis = dp.Basis(basis='pcseg-1')
water = dp.Molecule(input_file='data/water.xyz')

# --- undifferentiated
eri = dp.lsdalton.eri4(water, basis, hf)

S = dp.lsdalton.overlap_matrix(water, basis, hf)
k = dp.lsdalton.kinetic_energy_matrix(water, basis, hf)
h1 = dp.lsdalton.nuclear_electron_attraction_matrix(water, basis, hf)
h = k + h1

D = dp.lsdalton.diagonal_density(h, S, water, basis, hf)
J = dp.lsdalton.coulomb_matrix(D, water, basis, hf)
K = dp.lsdalton.exchange_matrix(D, water, basis, hf)

Jeri = np.einsum('ijkl, kl -> ij', eri, D)
Keri = np.einsum('ijkl, lj -> ik', eri, D)

print('Coulomb error', math.sqrt(np.einsum('ij, ji ->', J - Jeri, J - Jeri)))
print('exchange error', math.sqrt(np.einsum('ij, ji ->', K - Keri, K - Keri)))

F = h + 2.0*J + K

E = np.einsum('ij, ji -> ', D, 2*h + 2.0*J - K)
E_nuc = dp.lsdalton.nuclear_energy(water)
print('HF energy (from core hamiltonian starting guess):        ', E + E_nuc)
print('Electronic energy (from core hamiltonian starting guess):', E)
print('Nuclear repulsion energy:                                ', E_nuc)

# --- differentiated
eri_geo = dp.lsdalton.eri4(water, basis, hf, geometric_derivative_order=1)

S_geo = dp.lsdalton.overlap_matrix(water, basis, hf, geometric_derivative_order=1)
k_geo = dp.lsdalton.kinetic_energy_matrix(water, basis, hf, geometric_derivative_order=1)
h1_geo = dp.lsdalton.nuclear_electron_attraction_matrix(water, basis, hf, geometric_derivative_order=1)
J_geo = dp.lsdalton.coulomb_matrix(2 * D, water, basis, hf, geometric_derivative_order=1)
K_geo = dp.lsdalton.exchange_matrix(D, water, basis, hf, geometric_derivative_order=1)

J_eri_geo = np.einsum('gijkl, kl -> gij', eri_geo, D)
K_eri_geo = -np.einsum('gijkl, lj -> gik', eri_geo, D)
print('J_eri_geo error', np.einsum('gij, gij ->', J_geo - J_eri_geo, J_geo - J_eri_geo))
print('K_eri_geo error', np.einsum('gij, gij ->', K_geo - K_eri_geo, K_geo - K_eri_geo))
