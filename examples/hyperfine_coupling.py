import daltonproject as dp

mol = dp.Molecule(atoms='N 0 0 0')
bas = dp.Basis('Huz-IVsu4')
qc = dp.QCMethod('DFT', 'B3LYP')
prop = dp.Property(energy=True, hyperfine_couplings={'atoms': [1]})
result = dp.dalton.compute(mol, bas, qc, prop)
print(result.hyperfine_couplings)
