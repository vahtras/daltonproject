import daltonproject as dp

# Optional - set isotopes for each atom
# Most abundant isotopes are used by default
atom_isotopes = ['O17', 'H1', 'H1']

#  Essential settings
hf = dp.QCMethod(qc_method='HF', scf_threshold=1e-10)
basis = dp.Basis(basis='STO-3G')
molecule = dp.Molecule(input_file='data/water.xyz', isotopes=atom_isotopes)

# Compute settings
settings = dp.ComputeSettings(mpi_num_procs=1, jobs_per_node=1)

# optional - optimize the geometry
geo_opt = dp.Property(geometry_optimization=True)
opt_output = dp.dalton.compute(molecule, basis, hf, geo_opt, compute_settings=settings)
molecule.coordinates = opt_output.final_geometry

# Essential settings - compute the reference geometry Hessian
hess = dp.Property(hessian=True)
prop_output = dp.dalton.compute(molecule, basis, hf, hess, compute_settings=settings)
vib_prop = dp.Property(spin_spin_couplings=True)

# Essential settings for computing vibrational correction
va_settings = dp.VibAvSettings(
    molecule=molecule,
    property_program='dalton',
    is_mol_linear=False,
    hessian=prop_output.hessian,
    property_obj=vib_prop,
    stepsize=0.05,
    differentiation_method='polynomial',
    temperature=0,
    polynomial_fitting_order=3,
    # linear_point_stencil=3,
    plot_polyfittings=True)

# Essential settings - Instances of Molecule class for distorted geometries
molecules = []
for i in va_settings.file_list:
    molecules.append(dp.Molecule(input_file=i, isotopes=atom_isotopes))

# Essential settings - Compute Hessians at distorted gepmetries
dalton_results_hess = dp.dalton.compute_farm(molecules, basis, hf, hess, compute_settings=settings)

# Essential settings - Compute properties at distorted geometries
dalton_results_nmr = dp.dalton.compute_farm(molecules, basis, hf, vib_prop, compute_settings=settings)

# Essential settings - Instance of ComputeVibAvCorrection class containing the correction
result = dp.ComputeVibAvCorrection(dalton_results_hess, dalton_results_nmr, va_settings)

# print(result.cubic_force_constants)
# print(result.property_first_derivatives)
# print(result.property_second_derivatives)
# print(result.mean_displacement_q)
# print(result.mean_displacement_q2)
# print(result.vibrational_corrections)
