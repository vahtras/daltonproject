import numpy as np

import daltonproject as dp

hf = dp.QCMethod('HF')
basis = dp.Basis(basis='pcseg-1')
water = dp.Molecule(input_file='data/water.xyz')

density = dp.lsdalton.nuclear_electron_attraction_matrix(water, basis, hf)

points = np.array([[3.0, 0.0, 0.0], [0.0, 3.0, 0.0]])

# --- Point charges only

# Dimension (nPoints): the Coulomb interaction between the unit point charges
# located at C with the electron density represented by the AO denisty matrix
# density (D_ab): (C|\rho) = \sum_ab (C|ab) D_ab
ep_pot = dp.lsdalton.electrostatic_potential(density, points, water, basis, hf)

# Dimension (nbasis,nbasis): the interaction between AO products and the Coulomb
# potential of point charges q_C (here taken to be ep_pot): \sum_C q_C (C|ab)
ep_mat = dp.lsdalton.multipole_interaction_matrix(ep_pot, points, water, basis, hf)

# Dimension (nPoints,nbasis,nbasis): the integrals (C|ab)
ep_int = dp.lsdalton.electrostatic_potential_integrals(points, water, basis, hf)

# Equals to ep_pot
ep_pot2 = np.einsum('pij,ij', ep_int, 2*density) \
    + dp.lsdalton.nuclear_electrostatic_potential(points, water)

# Square sum of ep_pot errors
print(np.einsum('p, p -> ', ep_pot - ep_pot2, ep_pot - ep_pot2))

# Equals to ep_mat
ep_mat2 = np.einsum('pij,p', ep_int, ep_pot2)

# Square sum of ep_mat errors
print(np.einsum('ij, ji ->', ep_mat - ep_mat2, ep_mat - ep_mat2))

# --- Point charges and dipoles
EP = (0, 1)
# Dimension (4, nPoints): the Coulomb interaction between the unit point charges and dipoles
# located at C with the electron density represented by the AO denisty matrix
# density (D_ab): (C^lm|\rho) = \sum_ab (C^lm|ab) D_ab
ep_pot = dp.lsdalton.electrostatic_potential(density, points, water, basis, hf, ep_derivative_order=EP)

# Dimension (nbasis,nbasis): the interaction between AO products and the Coulomb
# potential of point and dipole charges q_C^lm (here taken to be ep_pot): \sum_C \sum_lm q_C^lm (C^lm|ab)
ep_mat = dp.lsdalton.multipole_interaction_matrix(ep_pot, points, water, basis, hf, multipole_orders=EP)

# Dimension (4,nPoints,nbasis,nbasis): the integrals (C^lm|ab)
ep_int = dp.lsdalton.electrostatic_potential_integrals(points, water, basis, hf, ep_derivative_order=EP)

# Equals to ep_pot
ep_pot2 = np.einsum('epij,ij', ep_int, 2*density) \
    + dp.lsdalton.nuclear_electrostatic_potential(points, water, ep_derivative_order=EP)

# Square sum of ep_pot errors
print(np.einsum('ep, ep -> ', ep_pot - ep_pot2, ep_pot - ep_pot2))

# Equals to ep_mat
ep_mat2 = np.einsum('epij,ep', ep_int, ep_pot)

# Square sum of ep_mat errors
print(np.einsum('ij, ij ->', ep_mat - ep_mat2, ep_mat - ep_mat2))
