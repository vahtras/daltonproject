import matplotlib.pyplot as plt

import daltonproject as dp

hf = dp.QCMethod(qc_method='HF')
basis = dp.Basis(basis='pcseg-0')
h2o2 = dp.Molecule(input_file='H2O2.xyz')

geo_opt = dp.Property(geometry_optimization=True)
opt_output = dp.lsdalton.compute(h2o2, basis, hf, geo_opt)
h2o2.coordinates = opt_output.final_geometry

props = dp.Property(hessian=True, dipole_gradients=True, polarizability_gradients={'frequencies': [0.07198]})
prop_output = dp.lsdalton.compute(h2o2, basis, hf, props)

vib_ana = dp.vibrational_analysis(molecule=h2o2,
                                  hessian=prop_output.hessian,
                                  dipole_gradients=prop_output.dipole_gradients,
                                  polarizability_gradients=prop_output.polarizability_gradients)

fig, ax1 = plt.subplots()
ax1 = dp.spectrum.plot_ir_spectrum(vib_ana, ax=ax1, color='blue', label='IR')
ax2 = ax1.twinx()
ax2 = dp.spectrum.plot_raman_spectrum(vib_ana, ax=ax2, color='red', label='Raman')
fig.legend(loc='center')
fig.savefig('ir_raman.svg')

import numpy as np  # noqa

assert np.allclose(vib_ana.frequencies,
                   [3774.18714494, 3770.35045183, 1577.93727375, 1216.70363359, 1093.29385969, 168.67330482])
assert np.allclose(vib_ana.ir_intensities, [
    27350933192839.098, 99356216.44618502, 36012.34878161766, 20228699865741.73, 13014.497033759582,
    61235204686595.484
])
assert np.allclose(
    vib_ana.raman_intensities,
    [[7.52596823e-32, 2.07883951e-26, 5.21002043e-27, 2.64905181e-35, 3.79938946e-26, 5.87606819e-36]])
