import daltonproject as dp

molecule, basis = dp.mol_reader('pyridine.mol')
molecule.analyze_symmetry()

mp2 = dp.QCMethod('MP2')
prop = dp.Property(energy=True)
mp2_result = dp.dalton.compute(molecule, basis, mp2, prop)
print(mp2_result.filename)
# b2773ec9e68d368d0c4b6889a5ec6299edc94101
print('MP2 energy =', mp2_result.energy)
# MP2 energy = -243.9889166118

import pathlib  # noqa

import numpy as np  # noqa

assert pathlib.Path(mp2_result.filename).name == 'b2773ec9e68d368d0c4b6889a5ec6299edc94101'
np.testing.assert_allclose(mp2_result.energy, -243.9889166118)
