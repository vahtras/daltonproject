import daltonproject as dp

#  Essential settings
hf = dp.QCMethod(qc_method='HF')
basis = dp.Basis(basis='STO-3G')
molecule = dp.Molecule(input_file='water.xyz')

# Settings if running multiple calculations in parallel using job farming
settings = dp.ComputeSettings(mpi_num_procs=1, jobs_per_node=1)

# optional - optimize the geometry
geo_opt = dp.Property(geometry_optimization=True)
opt_output = dp.dalton.compute(molecule, basis, hf, geo_opt, compute_settings=settings)
molecule.coordinates = opt_output.final_geometry

# Essential settings - compute the reference geometry Hessian
hess = dp.Property(hessian=True)
prop_output = dp.dalton.compute(molecule, basis, hf, hess, compute_settings=settings)
vib_prop = dp.Property(nmr_shieldings=True)

# Essential settings for computing vibrational correction
va_settings = dp.VibAvSettings(molecule=molecule,
                               property_program='dalton',
                               is_mol_linear=False,
                               hessian=prop_output.hessian,
                               property_obj=vib_prop,
                               stepsize=0.05,
                               differentiation_method='linear',
                               temperature=0,
                               linear_point_stencil=3)

# Essential settings - Instances of Molecule class for distorted geometries
molecules = []
for i in va_settings.file_list:
    molecules.append(dp.Molecule(input_file=i))

# Essential settings - Compute Hessians + property at distorted geometries
dalton_results_hess = []
dalton_results_nmr = []
for mol in molecules:
    dalton_results_hess.append(dp.dalton.compute(mol, basis, hf, hess, compute_settings=settings))
    dalton_results_nmr.append(dp.dalton.compute(mol, basis, hf, vib_prop, compute_settings=settings))

# Essential settings - Instance of ComputeVibAvCorrection class containing the correction
result = dp.ComputeVibAvCorrection(dalton_results_hess, dalton_results_nmr, va_settings)

print('\nVibrational Correction to Property:', result.vibrational_corrections)
# result.vibrational_correction = [-11.190194,  -0.388857,  -0.388853]

import numpy as np  # noqa

np.testing.assert_allclose(result.vibrational_corrections, [-11.190194, -0.388857, -0.388853], atol=1e-6)
