.. role:: python(code)
   :language: python

Vibrational Corrections to Properties
=====================================

In order to compare experimental calculations with
theoretical calculations at a high accuracy, one needs
to account for the vibrational motion of the nuclei.
With this implementation based on perturbation theory
to second order (VPT2), one can account for this
vibrational motion and correct the equilibrium
property value ad hoc. The vibrational correction
to the property :math:`P` is given as:

.. math::
    \Delta^\text{VPT2}P = -\frac{1}{2}\sum_i\frac{1}{\omega_i}\frac{\partial P}{\partial q_i}\sum_j{k_{ijj}}\left(\nu_j+\frac{1}{2}\right) + \frac{1}{2}\sum_{i}\left(\nu_i+\frac{1}{2}\right)\frac{\partial^2 P}{\partial q_i^2}

where :math:`k_{ijj}` is the cubic force constant in
:math:`\text{cm}^{-1}` and :math:`\omega` is the harmonic
frequency in :math:`\text{cm}^{-1}` with all derivatives
evaluated at :math:`q=0`. For a zero-point vibrational
correction (ZPVC), :math:`\nu` is set to 0. Temperature
dependent corrections with a rotational contribution
can also be computed using a Boltzmann averaging of
states:

.. math::
    \Delta^\text{VPT2}P = -\frac{1}{4}\sum_i\frac{1}{\omega_i}\frac{\partial P}{\partial q_i}\left(\sum_j{k_{ijj}}\coth\left(\frac{{h}c\omega_j}{2kT}\right) - \frac{kT}{2\pi c}\frac{1}{\sqrt{hc\omega_i}}\sum_\alpha \frac{a^{\alpha\alpha}_i}{I^e_{\alpha\alpha}}\right)
.. math::
    + \frac{1}{4}\sum_{i}\frac{\partial^2 P}{\partial q_i^2}\coth\left(\frac{{h}c\omega_i}{2kT}\right)


where :math:`T` is the temperature in Kelvin, :math:`k`
is the Boltzmann constant, :math:`h` is the Planck constant,
:math:`I` is the tensor of the principle axes of inertia
and :math:`a` is the tensor of linear expansion
coefficients of the moment of
inertia in the normal coordinates.
Cubic force constants, first and second derivatives of
properties are derived numerically from displaced geometry
calculations using either a 3/5 point stencil or a
polynomial fitting. The stepsize used for the numerical
analysis can be given as an argument and is
unitless (reduced normal coordinates).

The following properties are currently supported (program dependent):

    * NMR shieldings - Gaussian/Dalton
    * Spin-spin coupling constants - Gaussian/Dalton
    * Hyperfine coupling constants - Gaussian
    * Static polarizability/frequency-dependent polarizabilities
      - Dalton/Gaussian
    * Near static optical rotation/frequency-dependent optical rotations
      - Dalton/Gaussian

Tips:
    * Performing a geometry optimisation is always
      advised (unless providing an already optimised geometry).
    * The temperature can be given as an argument for
      calculations of temperature-dependent corrections.
    * The fitting method (linear/polynomial) can be changed
      to improve the accuracy of the results.
    * Temperature, fitting method, fitting order/point stencil
      can be changed, and the script can be rerun interactively
      without explicitly calling a quantum chemistry program.
    * If the stepsize is changed or the input geometry is changed
      in any way, then all files need to be deleted.
    * Polynomial fitting results can be inspected by changing
      the plot_polyfittings variable to "True" in the input file.
    * It is advised to create an output folder for the generated
      files and run the script from there (as many files can
      be generated!)

*Note, that the quantum chemistry methods used in this tutorial
may not be suitable for the particular problem that you want
to solve. Moreover, the basis set used here is minimal in order
to allow you to progress fast through the tutorial and should
under no circumstances be used in production calculations.*

Correction to NMR shieldings using a linear fitting
---------------------------------------------------

.. literalinclude:: shieldings_linear.py
   :lines: 1-47

To start, we will calculate corrections to NMR shieldings
for water. Here the cubic force constants and property
derivatives will be calculated using a 3-point linear
stencil with a stepsize of 0.05. An output file
containing all corrected shieldings will be generated.

Correction to spin-spin coupling constants using a polynomial fitting
---------------------------------------------------------------------

.. literalinclude:: sscc_polynomial.py
   :lines: 1-51

This time we will calculate corrections to the spin-spin coupling
constants for water. The cubic force constants and property
derivatives will be calculated using a 3rd-order polynomial
fitting with a stepsize of 0.05. Plots of the property
derivatives fitting can also be generated for inspection to
ensure the fitting is resonable. Here we also altered the
temperature to 298K to calculate the temperature-dependent
corrections. An output file containing all corrected
spin-spin coupling constants will be generated.

Graphs of the polynomial derivative fitting for each coupling
and associated mode is as shown:

.. image:: fc_corr_water.svg
   :width: 480

Correction to frequency-dependent polarizabilities using a linear fitting
-------------------------------------------------------------------------

.. literalinclude:: freq_dependent_polar.py
   :lines: 1-58

This time, we will calculate corrections to the frequency-dependent
polarizabilities for water where frequencies are in a.u.
Static polarizabilities are also done by default when frequency-dependent
polarizabilities are requested.

Optical rotations can be requested by changing

.. code-block:: python

  vib_prop = dp.Property(polarizabilities={'frequencies': [0.1, 0.2, 0.3]})

to

.. code-block:: python

  vib_prop = dp.Property(optical_rotations={'frequencies': [0.1, 0.2, 0.3]})

Ref:
Faber, R.; Kaminsky, J.; Sauer, S. P. A. In Gas Phase NMR, Jackowski, K.,
Jaszunski, M., Eds.; Royal Society of Chemistry, London: 2016; Chapter 7,
pp 218–266
