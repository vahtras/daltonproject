import daltonproject as dp

# Optional - set isotopes for each atom
atom_isotopes = ['O17', 'H1', 'H1']

#  Essential settings
hf = dp.QCMethod(qc_method='HF')
basis = dp.Basis(basis='STO-3G')
molecule = dp.Molecule(input_file='water.xyz', isotopes=atom_isotopes)

# Compute settings
settings = dp.ComputeSettings(mpi_num_procs=1, jobs_per_node=1)

# optional - optimize the geometry
geo_opt = dp.Property(geometry_optimization=True)
opt_output = dp.dalton.compute(molecule, basis, hf, geo_opt, compute_settings=settings)
molecule.coordinates = opt_output.final_geometry

# Essential settings - compute the reference geometry Hessian
hess = dp.Property(hessian=True)
prop_output = dp.dalton.compute(molecule, basis, hf, hess, compute_settings=settings)
vib_prop = dp.Property(spin_spin_couplings=True)

# Essential settings for computing vibrational correction
va_settings = dp.VibAvSettings(molecule=molecule,
                               property_program='dalton',
                               is_mol_linear=False,
                               hessian=prop_output.hessian,
                               property_obj=vib_prop,
                               stepsize=0.05,
                               differentiation_method='polynomial',
                               temperature=298,
                               polynomial_fitting_order=3,
                               plot_polyfittings=True)

# Essential settings - Instances of Molecule class for distorted geometries
molecules = []
for i in va_settings.file_list:
    molecules.append(dp.Molecule(input_file=i, isotopes=atom_isotopes))

# Essential settings - Compute Hessians + property at distorted geometries
dalton_result_hess = []
dalton_result_nmr = []
for mol in molecules:
    dalton_result_hess.append(dp.dalton.compute(mol, basis, hf, hess, compute_settings=settings))
    dalton_result_nmr.append(dp.dalton.compute(mol, basis, hf, vib_prop, compute_settings=settings))

# Essential settings - Instance of ComputeVibAvCorrection class containing the correction
result = dp.ComputeVibAvCorrection(dalton_result_hess, dalton_result_nmr, va_settings)

print('\nVibrational Correction to Property:', result.vibrational_corrections)
# result.vibrational_correction =  [4.403487,  4.403505, -5.387707]

import numpy as np  # noqa

np.testing.assert_allclose(result.vibrational_corrections, [4.403487, 4.403505, -5.387707], atol=1e-6)
