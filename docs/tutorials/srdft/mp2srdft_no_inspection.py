import daltonproject as dp

mps2srdft_output = dp.dalton.OutputParser('282603d082ffdea3ff8ef1432f5dfe1f69c12db7')
nat_occs = mps2srdft_output.natural_occupations
print(dp.natural_occupation.scan_occupations(nat_occs))

scan_nat_occs = dp.natural_occupation.scan_occupations(nat_occs)
assert scan_nat_occs == 'Strongly occupied natural orbitals                      Weakly occupied natural orbitals' \
                        '\nSymmetry   Occupation   Change in occ.   Diff. to 2     Symmetry   Occupation   Change' \
                        ' in occ.\n   4          1.9879        0.0000        0.0121            2          0.0124 ' \
                        '       0.0000\n   2          1.9882        0.0003        0.0118            4          ' \
                        '0.0120        0.0004\n---------------------------------------------------         3      ' \
                        '    0.0026        0.0094\n---------------------------------------------------         1   ' \
                        '       0.0023        0.0003\n---------------------------------------------------         1' \
                        '          0.0020        0.0002\n'
