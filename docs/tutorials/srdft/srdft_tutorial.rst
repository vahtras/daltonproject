.. role:: python(code)
   :language: python

Short-Range Density Functional Theory
=====================================

In the short-range density functional theory model (srDFT), the total energy
is calculated as a long-range interaction by wavefunction theory and a
short-range by density functional theory. The philosophy of this method is to
use the good semi-local description of DFT, and then describe the long-range
with wavefunction methods.

.. math::
    E_\text{WF-srDFT} = \left<\Psi^\text{lr}\left|-\frac{1}{2}\nabla^2 + \frac{g(r_{12},\mu)}{r_{12}}\right|\Psi^\text{lr}\right> + E_\text{Hxc}^\text{sr}\left[\rho_{\Psi^\text{lr}}\right] + \int_\Omega \rho_{\Psi^\text{lr}}(r) v_\text{ext}(r) \mathrm{d}r

Here :math:`g(r_{12},\mu)` is a range-separation function, with a
range-separation parameter :math:`\mu`.
The usual form of the range-separation function is,

.. math::
   g(r_{12},\mu) = \frac{\mathrm{erf}(\mu r_{12})}{r_{12}}

It can be noted that KS-DFT and regular wavefunction theory is recovered in
the limiting cases of :math:`\mu \rightarrow 0` and
:math:`\mu \rightarrow \infty`, respectively.

.. math::
    \lim_{\mu\rightarrow 0} E_\text{WF-srDFT} = \left<\phi\left|-\frac{1}{2}\nabla^2\right|\phi\right> + E_\text{Hxc}\left[\rho_{\phi}\right] + \int_\Omega \rho_{\phi}(r) v_\text{ext}(r) \mathrm{d}r = E_\text{KS-DFT}

and,

.. math::
    \lim_{\mu\rightarrow \infty} E_\text{WF-srDFT} = \left<\Psi\left|-\frac{1}{2}\nabla^2 + \frac{1}{r_{12}}\right|\Psi\right> + \int_\Omega \rho_{\Psi}(r) v_\text{ext}(r) \mathrm{d}r = E_\text{WF}

HF--srDFT
---------

In Hartree--Fock srDFT both the exchange and correlation functional are
range-separated. This makes HF--srDFT similar to long-range corrected DFT
(LRC-DFT) with the difference that only the exchange functional is
range-separated in LRC-DFT. An HF--srDFT calculation can be run as:

.. literalinclude:: hfsrdft.py
   :lines: 1-9
   :emphasize-lines: 4-5

The specification :python:`QCMethod('HFsrDFT', 'SRPBEGWS')` signifies that an
HF--srDFT calculation will be performed with the short-range PBE functional of
Goll, Werner and, Stoll. Furthermore,
:python:`range_separation_parameter(0.4)` sets the range-separation parameter
to :math:`0.4\ \mathrm{bohr^{-1}}`, which is the recommended value.

The HF--srDFT method can also be used to do LRC-DFT calculations,
in which case the range-separation is only applied to the exchange fucntional.

.. literalinclude:: lrcdft.py
   :lines: 1-9
   :emphasize-lines: 4

The setup is the same as for the HF--srDFT calculation but the functional
have been changed :python:`QCMethod('HFsrDFT', 'LRCPBEGWS')`.


MP2--srDFT
----------

MP2 with srDFT is also supported. The main reason to use this method is as an
orbital generator for CAS--srDFT calculations. MP2--srDFT is setup very
similar to HF--srDFT.

.. literalinclude:: mp2srdft.py
   :lines: 1-12
   :emphasize-lines: 5

Note here, that MP2srDFT have been specified instead of HFsrDFT
:python:`QCMethod('MP2srDFT', 'SRPBEGWS')`.

If the natural orbital occupations are inspected it can be seen
that they are closer to 2 or 0 than for the full-range MP2 in
:ref:`Picking CAS based on Natural Orbital Occupations`.

.. literalinclude:: mp2srdft_no_inspection.py
   :lines: 1-5

Inspecting the natural orbital occupations uses the same
methods as for full-range MP2.

.. code-block:: text
    :emphasize-lines: 3-4

    Strongly occupied natural orbitals                      Weakly occupied natural orbitals
    Symmetry   Occupation   Change in occ.   Diff. to 2     Symmetry   Occupation   Change in occ.
       4          1.9879        0.0000        0.0121            2          0.0124        0.0000
       2          1.9882        0.0003        0.0118            4          0.0120        0.0004
    ---------------------------------------------------         3          0.0026        0.0094
    ---------------------------------------------------         1          0.0023        0.0003
    ---------------------------------------------------         1          0.0020        0.0002

CAS--srDFT
----------

Doing CAS calculations with srDFT also requires the choosing of an
active-space. The same methods presented in :ref:`Complete Active Space
Self-Consistent Field` can be used for CAS--srDFT also. As an example of an
CAS--srDFT calculation, lets start with the MP2 natural orbitals calculated
in :ref:`MP2--srDFT`.

.. literalinclude:: cassrdft.py
   :lines: 1-15
