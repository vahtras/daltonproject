# Dalton Project

The Dalton Project is a Python platform for molecular- and electronic-structure
simulations of complex systems. Documentation can be found at
[https://daltonproject.rtfd.io/](https://daltonproject.rtfd.io/)


## Copyright and License

Copyright (C)  The Dalton Project Developers.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact information can be found on our website: https://www.daltonproject.org/


## Citation

We hope that you will cite us in any publication in which results were obtained
in whole or in part based on this software. A suitable citation for the software
is:

> The Dalton Project Developers, _Dalton Project: A Python platform for molecular-
and electronic-structure simulations of complex systems_ (version 0.1a0), 2022.
DOI: 10.5281/zenodo.3688799. Available at https://www.daltonproject.org/.

In addition, we ask that you cite the
[Dalton Project article](https://doi.org/10.1063/1.5144298)
which describes our purpose, design strategy, and vision.
Finally, you should also cite software used through the Dalton Project.


## Installation

Dalton Project can be conveniently downloaded and installed using **pip**:

    $ python -m pip install git+https://gitlab.com/daltonproject/daltonproject


## Usage

The following snippet shows an example of how the Dalton Project can be used to
compute two-photon absorption cross-sections employing the interface to the
Dalton program:

    >>> import daltonproject as dp
    >>> dft = dp.QCMethod('DFT', 'B3LYP')
    >>> basis = dp.Basis(basis='pcseg-0')
    >>> ethanol = dp.Molecule(xyz='examples/data/ethanol.xyz', charge=0)
    >>> property = dp.Property(two_photon_absorption=True)
    >>> result = dp.dalton.compute(ethanol, basis, dft, property)
    >>> for energy, cross_section in zip(result.excitation_energies, result.two_photon_cross_sections):
    ...     print(f"{energy:6.3f} {cross_section:6.3f}")
     7.177  0.143
     8.497  0.965
     8.926  0.293
     9.449  1.300
     9.620  0.727

Note that this assumes that the path to the Dalton executable is in the *PATH*
environment variable. More examples are located in the *examples* directory.


## Developers

Developers can clone the Dalton Project git repository which is hosted on
GitLab (https://gitlab/daltonproject/daltonproject):

    $ git clone https://gitlab.com/daltonproject/daltonproject.git

Alternatively, you can fork the project on GitLab and the clone the fork. After
the clone is downloaded you change directory to the root of the Dalton
Project source:

    $ cd daltonproject

If want to work within a virtual environment, then the first step is to create
one (arbitrarily named `dp-dev-env`) and activate it:

    $ python -m venv dp-dev-env
    $ . dp-dev-env/bin/activate

Then install the Dalton Project package in development mode which allows you to
try out your changes immediately using the installed package:

    $ python -m pip install --editable .[tests,docs]

This will also install dependencies needed to use the Dalton Project, to run
the test suite, and to build the documentation, unless they are already
installed. Note that this will interfere with/overwrite any **pip** installed
version of the Dalton Project. To instead install versions of the required
dependencies that we have tested and are known to be compatible, run the
following commands:

    $ python -m pip install -r requirements.txt
    $ python -m pip install -r tests/requirements.txt

Many both newer and older versions are likely to work as well. Using
`python -m` as above is recommended when not working within a virtual
environment but can safely be omitted if you are working within a virtual
environment.

Before making any changes, make sure that all tests pass by running the test
suite (integration tests require that the path to external libraries are in the
*PATH* environment variable):

    $ python -m pytest

The code linting and formatting tools can be setup to work automatically via
pre-commit hooks. To setup the pre-commit hooks run the following from the
root of the Dalton Project directory:

    $ python -m pip install pre-commit
    $ pre-commit install

During a **git commit**, if any pre-commit hook fails, mostly you will simply
need to **git add** the affected files and **git commit** again, because some
tools will automatically reformat the files.

To build the documentation, there are additional dependencies that can be
installed either when you are installing the Dalton Project (as shown above)
or as follows to install specific versions that have been tested:

    $ python -m pip install -r docs/requirements.txt

To build the documentation, run:

    $ sphinx-build docs output_docs

where `output_docs` is an arbitrarily named directory where the compiled
documentation will be located.
