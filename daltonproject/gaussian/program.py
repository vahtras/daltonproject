"""Interface to the Gaussian16 program that sets up and runs calculations through subprocess."""
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from __future__ import annotations  # Delete when Python 3.10 is oldest supported version

import logging
import multiprocessing as mp
import os
import re
import shutil
import tempfile
import warnings
from collections import defaultdict
from collections.abc import Sequence

import parse

from ..basis import Basis
from ..environment import Environment
from ..molecule import Molecule
from ..program import ComputeSettings, Program
from ..property import Property
from ..qcmethod import QCMethod
from ..utilities import chdir, create_sbatch_command, job_farm, num_cpu_cores, run, unique_filename, worker
from .output_parser import OutputParser

logger = logging.getLogger('daltonproject')


class Gaussian(Program):
    """Interface to the Gaussian program"""

    program_name: str = 'g16'

    @classmethod
    def compute(cls,
                molecule: Molecule,
                basis: Basis,
                qc_method: QCMethod,
                properties: Property,
                environment: Environment | None = None,
                compute_settings: ComputeSettings | None = None,
                filename: str | None = None,
                force_recompute: bool = False) -> OutputParser:
        """Run a calculation using the Gaussian program.

        Args:
            molecule: Molecule on which a calculations is performed. This can
                also be an atom, a fragment, or any collection of atoms.
            basis: Basis set to use in the calculation.
            qc_method: Quantum chemistry method, e.g., HF, DFT, or CC, and
                associated settings.
            properties: Properties of molecule to be calculated, geometry
                optimization, excitation energies, etc.
            environment: Environment description missing
            compute_settings: Settings for the calculation, e.g., number of MPI
                processes and OpenMP threads, work and scratch directory, etc.
            filename: Optional user-specified filename that will be used for
                input and output files. If not specified a name will be
                generated as a hash of the input.
            force_recompute: Recompute even if the output files already exist.

        Returns:
            OutputParser instance that contains the filename of the output
            produced in the calculation and can be used to extract results from
            the output.
        """
        Program._validate_compute_args(molecule, basis, qc_method, properties, environment, compute_settings,
                                       filename, force_recompute)
        if compute_settings is None:
            mpi_num_procs = num_cpu_cores()
            compute_settings = ComputeSettings(mpi_num_procs=mpi_num_procs)
        mpi_procs_per_node = compute_settings.mpi_num_procs // len(compute_settings.node_list)
        mpi_memory = compute_settings.memory // mpi_procs_per_node
        mol_input = molecule_input(molecule)
        gaus_input = gaussian_input(basis, qc_method, properties, mpi_memory, compute_settings.mpi_num_procs)
        extra_input = extra_gaussian_input(properties)
        if environment:
            pass
        if not filename:
            input_list = [mol_input, gaus_input, extra_input]
            filename = unique_filename(input_list)
        gaus_input = gaussian_input(basis, qc_method, properties, mpi_memory, compute_settings.mpi_num_procs,
                                    filename)
        filepath = os.path.join(compute_settings.work_dir, filename)
        if os.path.isfile(f'{filepath}.out') and os.path.isfile(f'{filepath}.fchk') and not force_recompute:
            logger.info('Output files already exist and will be reused.')
            return OutputParser(filepath)
        if shutil.which(cmd=cls.program_name) is None:
            raise FileNotFoundError(f'The {cls.program_name} script was not found or is not executable.')
        if shutil.which(cmd='formchk') is None:
            raise FileNotFoundError('The formchk script was not found or is not executable.')
        with chdir(compute_settings.work_dir):
            with open(f'{filename}.gjf', 'w+') as gaus_file:
                gaus_file.write(gaus_input)
                gaus_file.write(mol_input)
                gaus_file.write(extra_input)
            basis.write()
            if environment:
                pass
            os.environ['GAUSS_SCRDIR'] = compute_settings.scratch_dir
            command = (f'{cls.program_name}'
                       f' {filename}.gjf'
                       f' > {filename}.out;'
                       f'formchk -3 {filename}.chk {filename}.fchk')
            if compute_settings.slurm:
                result_queue: mp.Queue[tuple[str, str, int]] = mp.Queue()
                sbatch_command = create_sbatch_command(compute_settings.slurm_partition,
                                                       compute_settings.slurm_walltime,
                                                       compute_settings.slurm_account,
                                                       compute_settings.mpi_num_procs,
                                                       compute_settings.omp_num_threads, compute_settings.memory)
                sbatch_command += f' --job-name=slurm_{filename} -o slurm_{filename}.out -e slurm_{filename}.err'
                with tempfile.NamedTemporaryFile(mode='x+t', delete=False) as temp_file:
                    temp_file.write('#!/bin/bash\n')
                    temp_file.write(command)
                    process = mp.Process(target=worker, args=(f'{sbatch_command} {temp_file.name}', result_queue))
                    process.start()
                while True:
                    if result_queue.qsize() == 1:
                        break
                result = result_queue.get()
                stdout, stderr, return_code = result
            else:
                stdout, stderr, return_code = run(command)
        if return_code != 0:
            result = parse.search('Reason: {}\n', stderr)
            if result is None:
                error_message = stderr
            else:
                error_message = result[0]
            raise Exception(f'Gaussian exited with non-zero return code. Error message:\n{error_message}')
        return OutputParser(filepath)

    @classmethod
    def compute_farm(cls,
                     molecules: Sequence[Molecule],
                     basis: Basis,
                     qc_method: QCMethod,
                     properties: Property,
                     compute_settings: ComputeSettings | None = None,
                     filenames: Sequence[str] | None = None,
                     force_recompute: bool = False) -> list[OutputParser]:
        """Run a series of calculations using the Gaussian program.

        Args:
            molecules: List of molecules on which calculations are performed.
            basis: Basis set to use in the calculations.
            qc_method: Quantum chemistry method, e.g., HF, DFT, or CC, and
                associated settings used for all calculations.
            properties: Properties of molecule to be calculated, geometry
                optimization, excitation energies, etc., computed for all
                molecules.
            compute_settings: Settings for the calculations, e.g., number of
                MPI processes and OpenMP threads, work and scratch directory,
                and more.
            filenames: Optional list of user-specified filenames that will be
                used for input and output files. If not specified names will be
                generated as a hash of the individual inputs.
            force_recompute: Recompute even if the output files already exist.

        Returns:
            List of OutputParser instances each of which contains the filename
            of the output produced in the calculation and can be used to
            extract results from the output.
        """
        if compute_settings is None:
            mpi_num_procs = num_cpu_cores()
            compute_settings = ComputeSettings(mpi_num_procs=mpi_num_procs)
        mpi_procs_per_node = compute_settings.mpi_num_procs // len(compute_settings.node_list)
        mpi_memory = compute_settings.memory // mpi_procs_per_node
        mol_inputs = [molecule_input(molecule) for molecule in molecules]
        gaus_input = gaussian_input(basis, qc_method, properties, mpi_memory, compute_settings.mpi_num_procs)
        extra_input = extra_gaussian_input(properties)
        if not filenames:
            input_lists = [[mol_input, gaus_input, extra_input] for mol_input in mol_inputs]
            filenames = [unique_filename(input_list) for input_list in input_lists]
        with chdir(compute_settings.work_dir):
            commands = []
            output_parsers = []
            run_files = []
            sbatch_command = None
            for filename, mol_input in zip(filenames, mol_inputs):
                output_parsers.append(OutputParser(os.path.join(compute_settings.work_dir, filename)))
                gaus_input = gaussian_input(basis, qc_method, properties, mpi_memory,
                                            compute_settings.mpi_num_procs, filename)
                if os.path.isfile(f'{filename}.out') and os.path.isfile(f'{filename}.fchk') and not force_recompute:
                    continue
                if shutil.which(cmd=cls.program_name) is None:
                    raise FileNotFoundError(f'The {cls.program_name} script was not found or is not executable.')
                if shutil.which(cmd='formchk') is None:
                    raise FileNotFoundError('The formchk script was not found or is not executable.')
                with open(f'{filename}.gjf', 'w') as gaus_file:
                    gaus_file.write(gaus_input)
                    gaus_file.write(mol_input)
                    gaus_file.write(extra_input)
                basis.write()
                os.environ['GAUSS_SCRDIR'] = compute_settings.scratch_dir
                command = (f' {cls.program_name}'
                           f'  {filename}.gjf'
                           f' > {filename}.out;'
                           f' formchk -3 {filename}.chk {filename}.fchk')
                commands.append(command)
                run_files.append(filename)
            if compute_settings.slurm:
                sbatch_command = create_sbatch_command(compute_settings.slurm_partition,
                                                       compute_settings.slurm_walltime,
                                                       compute_settings.slurm_account,
                                                       compute_settings.mpi_num_procs,
                                                       compute_settings.omp_num_threads, compute_settings.memory)
            results = job_farm(commands, compute_settings.node_list, compute_settings.jobs_per_node,
                               compute_settings.comm_port, sbatch_command, run_files)
            for stdout, stderr, return_code in results:
                if return_code != 0:
                    result = parse.search('Reason: {}\n', stderr)
                    if result is None:
                        error_message = stderr
                    else:
                        error_message = result[0]
                    warnings.warn(f'Gaussian exited with non-zero return code. Error message:\n{error_message}')
        return output_parsers


def molecule_input(molecule: Molecule) -> str:
    """Generate Gaussian molecule input without using molecular symmetry.

    Args:
        molecule: Molecule class object.

    Returns:
        String that contains Gaussian molecule input.
    """
    coordinate_groups = []
    element_group = []
    isotopes_group = []
    for element, coordinate, isotope in zip(molecule.elements, molecule.coordinates, molecule.isotopes):
        isotopes_group.append(re.sub('\\D', '', isotope))
        element_group.append(element)
        coordinate_groups.append(coordinate)
    mol_input = ''
    mol_input += f'{molecule.charge}  {molecule.multiplicity} \n'
    for element, coordinate, iso in zip(element_group, coordinate_groups, isotopes_group):
        mol_input += f'{element}(iso={iso}) {coordinate[0]:12.10f} {coordinate[1]:12.10f} {coordinate[2]:12.10f}\n'
    mol_input += '\n'
    return mol_input


def extra_gaussian_input(properties: Property) -> str:
    """Extra settings for Gaussian input that proceed the molecular geometry section.

    Args:
        properties: Property class object.

    Returns:
        extra_input: String that contains Gaussian input.
    """
    extra_input = ''
    if 'polarizabilities_frequencies' in properties.settings:
        for i in range(len(properties.settings['polarizabilities_frequencies'])):
            extra_input += str(properties.settings['polarizabilities_frequencies'][i]) + '\n'
    elif 'optical_rotations_frequencies' in properties.settings:
        for i in range(len(properties.settings['optical_rotations_frequencies'])):
            extra_input += str(properties.settings['optical_rotations_frequencies'][i]) + '\n'
    extra_input += '\n'
    return extra_input


def gaussian_input(basis: Basis,
                   qc_method: QCMethod,
                   properties: Property,
                   memory: int,
                   mpi_num_procs: int,
                   filename: str | None = None) -> str:
    """Generate Gaussian input as a string.

    Args:
        molecule: Molecule
        qc_method: QCMethod object.
        properties: Property object.
        memory: Memory in MB.
        mpi_num_procs: Number of MPI processes.
        filename: Filename for the input file.

    Returns:
        String of Gaussian input.
    """

    def input_tree() -> defaultdict:
        """Create a recursive defaultdict structure.

        When accessing an unbound value, a nested dictionary is created,
        with the final value initialized to an empty defaultdict(input_tree).

        Returns:
            Recursive defaultdict.
        """
        return defaultdict(input_tree)

    sections = input_tree()
    if filename is not None:
        sections['%chk='] = f'{filename}.chk\n'
    sections['%mem='] = f'{memory}MB\n'
    sections['%nprocs='] = f'{mpi_num_procs}\n'
    if 'nmr_shieldings' in properties.settings:
        sections['# NMR']
    if 'spin_spin_couplings' in properties.settings:
        sections['# NMR=SpinSpin']
    if 'geometry_optimization' in properties.settings:
        sections['# Opt']
    if 'hessian' in properties.settings:
        sections['# Freq']
    if 'polarizabilities' in properties.settings and 'polarizabilities_frequencies' in properties.settings:
        sections['# Polar CPHF=RdFreq']
    elif 'polarizabilities' in properties.settings:
        sections['# Polar']
    if 'optical_rotations' in properties.settings:
        sections['# Polar=OptRot CPHF=RdFreq']
    if 'hyperfine_couplings' in properties.settings or 'energy' in properties.settings:
        sections['# ']
    if qc_method.settings['qc_method'] == 'HF':
        sections[f' HF/{basis.basis}']
    elif qc_method.settings['qc_method'] == 'DFT':
        sections[' ' + qc_method.settings['xc_functional'] + '/' + basis.basis]
    elif qc_method.settings['qc_method'] == 'MP2':
        sections[f' MP2/{basis.basis}']
    if 'scf_threshold' in qc_method.settings:
        sections[' SCF=(Conver='] = str(qc_method.settings['scf_threshold']).split('-')[1] + ')'
    if 'symmetry' not in qc_method.settings:
        sections[' Symmetry=None']

    def parse_input_tree(section_tree: defaultdict, input_string: str = '') -> str:
        """Parse section tree and convert to Gaussian input string.

        Args:
            section_tree: A nested dict of dicts, which contains the activated
                gaussian sections, subsections, keywords and values.
            input_string: The string which becomes the final gaussian input.

        Returns:
            String of Gaussian input. first half of route section
        """
        for key, value in section_tree.items():
            if value:
                input_string += key + str(value)
            if key in input_string:
                continue
            else:
                input_string += key
        input_string += '\n\nGaussian input file generated by Dalton Project\n'
        input_string += '\n'
        return input_string

    gaus_input = parse_input_tree(sections)
    return gaus_input
