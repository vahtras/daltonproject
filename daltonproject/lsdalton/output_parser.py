"""Parse LSDalton output files."""
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from __future__ import annotations  # Delete when Python 3.10 is oldest supported version

import re
from typing import Any

import numpy as np
from qcelemental import PhysicalConstantsContext

from .. import program
from ..program import PolarizabilityGradients

constants = PhysicalConstantsContext('CODATA2018')


class OutputParser(program.OutputParser):
    """Parse LSDalton output files."""

    def __init__(self, filename: str) -> None:
        """Initialize LSDalton output parser."""
        self._filename = filename

    @property
    def filename(self) -> str:
        """Name of the LSDalton output files without the extension."""
        return self._filename

    @property
    def energy(self) -> float:
        """Extract the final energy from LSDalton output file."""
        with open(f'{self.filename}.out', 'r') as output_file:
            match = re.findall(pattern=r'\n.*Final.*energy: +(.*)\n', string=output_file.read())
        if not match:
            raise Exception(f'Energy not found in {self.filename}.')
        return float(match[-1])

    @property
    def electronic_energy(self) -> float:
        """Extract the electronic energy from LSDalton output file."""
        with open(f'{self.filename}.out', 'r') as output_file:
            match = re.findall(pattern=r'\n.*Electronic energy: +(.*)\n', string=output_file.read())
        if not match:
            raise Exception(f'Electronic energy not found in {self.filename}.')
        return float(match[-1])

    @property
    def nuclear_repulsion_energy(self) -> float:
        """Extract the nuclear repulsion energy from LSDalton output file."""
        with open(f'{self.filename}.out', 'r') as output_file:
            match = re.findall(pattern=r'\n.*Nuclear repulsion: +(.*)\n', string=output_file.read())
        if not match:
            raise Exception(f'Nuclear repulsion energy not found in {self.filename}.')
        return float(match[-1])

    @property
    def gradients(self) -> np.ndarray:
        """Extract molecular gradient from LSDalton output file."""
        with open(f'{self.filename}.out', 'r') as output_file:
            lines = output_file.readlines()
        for line in lines:
            if 'Total number of atoms        :' in line:
                num_atoms = int(line.split()[5])
                break
        else:
            raise Exception(f'Number of atoms not found in {self.filename}.')
        gradients = np.zeros((num_atoms, 3))
        gradient_start = 0
        for i, line in enumerate(lines):
            if 'Molecular gradient (au)' in line:
                gradient_start = i + 3
        if gradient_start == 0:
            raise Exception(f'Molecular gradients not found in {self.filename}.')
        for i, line in enumerate(lines[gradient_start:gradient_start + num_atoms]):
            gradients[i, :] = np.array(line.split()[-3:]).astype(np.float64)
        return gradients

    @property
    def hessian(self) -> np.ndarray:
        """Extract molecular hessian from OpenRSP tensor file."""
        rsp_tensor = read_openrsp_tensor(self.filename)
        for prop in rsp_tensor['properties']:
            if prop['operators'] == ['GEO', 'GEO']:
                hessian = prop['values'][0]
                break
        else:
            raise Exception('Hessian not found.')
        hessian = np.triu(hessian) + np.tril(hessian.T, -1)
        return hessian

    @property
    def dipole_gradients(self) -> np.ndarray:
        """Extract Cartesian dipole gradients from OpenRSP tensor file."""
        rsp_tensor = read_openrsp_tensor(self.filename)
        for prop in rsp_tensor['properties']:
            if prop['operators'] == ['GEO', 'EL']:
                dipole_gradients = prop['values'][0]
                break
        else:
            raise Exception('Dipole gradients not found.')
        return dipole_gradients

    @property
    def polarizability_gradients(self) -> PolarizabilityGradients:
        """Extract Cartesian polarizability gradients from OpenRSP tensor file."""
        rsp_tensor = read_openrsp_tensor(self.filename)
        for prop in rsp_tensor['properties']:
            if prop['operators'] == ['GEO', 'EL', 'EL']:
                all_gradients = prop['values']
                all_frequencies = prop['frequencies']
                break
        else:
            raise Exception('Polarizability gradients not found.')
        frequencies = np.array([frequency[-1] for frequency in all_frequencies])
        for gradients in all_gradients:
            for i in range(gradients.shape[0]):
                gradients[i, :, :] = np.triu(gradients[i, :, :]) + np.tril(gradients[i, :, :].T, -1)
        return PolarizabilityGradients(frequencies=frequencies, values=np.array(all_gradients))

    @property
    def final_geometry(self) -> np.ndarray:
        """Extract final geometry from LSDalton output file."""
        with open(f'{self.filename}.molecule_out.xyz', 'r') as xyz_file:
            xyz_output = xyz_file.read()
        lines = xyz_output.split('\n')
        num_atoms = int(lines[0])
        coordinates = []
        for line in lines[2:num_atoms + 2]:
            split_line = line.split()
            coordinates.append([float(value) for value in split_line[1:4]])
        return np.array(coordinates)

    @property
    def excitation_energies(self) -> np.ndarray:
        """Extract one-photon excitation energies from LSDalton output file."""
        excitation_energies = []
        with open(f'{self.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'Excitation              Transition Dipole Moments               Oscillator' in line:
                    output_file.readline()
                    output_file.readline()
                    excitation = output_file.readline().split()
                    while excitation:
                        excitation_energy = float(excitation[0]) * constants.hartree2ev
                        excitation_energies.append(excitation_energy)
                        excitation = output_file.readline().split()
        return np.array(excitation_energies).astype(np.float64)

    @property
    def oscillator_strengths(self) -> np.ndarray:
        """Extract one-photon oscillator strengths from LSDalton output file."""
        oscillator_strengths = []
        with open(f'{self.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'Excitation              Transition Dipole Moments               Oscillator' in line:
                    output_file.readline()
                    output_file.readline()
                    excitation = output_file.readline().split()
                    while excitation:
                        oscillator_strengths.append(excitation[4])
                        excitation = output_file.readline().split()
        return np.array(oscillator_strengths).astype(np.float64)


def read_openrsp_tensor(filename: str) -> dict[str, Any]:
    """Read OpenRSP tensor file."""
    rsp_tensor = {}
    with open(f'{filename}.rsp_tensor') as f:
        if f.readline().strip() != 'VERSION':
            raise Exception('LSDalton / OpenRSP file is incomplete.')
        rsp_tensor['version'] = int(f.readline())
        if f.readline().strip() != 'NUM_PROPERTIES':
            raise Exception('LSDalton / OpenRSP file is incomplete.')
        num_properties = int(f.readline())
        rsp_tensor['num_properties'] = num_properties
        rsp_tensor['properties'] = []  # type: ignore
        for i in range(num_properties):
            rsp_tensor['properties'].append({})  # type: ignore
            if f.readline().strip() != 'NEW_PROPERTY':
                raise Exception('LSDalton / OpenRSP file is incomplete.')
            if f.readline().strip() != 'ORDER':
                raise Exception('LSDalton / OpenRSP file is incomplete.')
            order = int(f.readline())
            rsp_tensor['properties'][i].update({'order': order})  # type: ignore
            if f.readline().strip() != 'NUM_FREQ_CFGS':
                raise Exception('LSDalton / OpenRSP file is incomplete.')
            num_freq_cfgs = int(f.readline())
            rsp_tensor['properties'][i].update({'num_freq_cfgs': num_freq_cfgs})  # type: ignore
            if f.readline().strip() != 'OPERATORS':
                raise Exception('LSDalton / OpenRSP file is incomplete.')
            rsp_tensor['properties'][i].update({'operators': []})  # type: ignore
            for j in range(order):
                rsp_tensor['properties'][i]['operators'].append(f.readline().strip())  # type: ignore
            if f.readline().strip() != 'NUM_COMPONENTS':
                raise Exception('LSDalton / OpenRSP file is incomplete.')
            rsp_tensor['properties'][i].update({'num_components': []})  # type: ignore
            for j in range(order):
                rsp_tensor['properties'][i]['num_components'].append(int(f.readline()))  # type: ignore
            if f.readline().strip() != 'FREQUENCIES':
                raise Exception('LSDalton / OpenRSP file is incomplete.')
            rsp_tensor['properties'][i].update({'frequencies': []})  # type: ignore
            for j in range(num_freq_cfgs):
                if f.readline().strip() != 'CONFIGURATION':
                    raise Exception('LSDalton / OpenRSP file is incomplete.')
                rsp_tensor['properties'][i]['frequencies'].append([])  # type: ignore
                for _ in range(order):
                    rsp_tensor['properties'][i]['frequencies'][j].append(float(f.readline()))  # type: ignore
            if f.readline().strip() != 'VALUES':
                raise Exception('LSDalton / OpenRSP file is incomplete.')
            shape = rsp_tensor['properties'][i]['num_components']  # type: ignore
            rsp_tensor['properties'][i].update({'values': []})  # type: ignore
            max_loop = int(np.prod(rsp_tensor['properties'][i]['num_components']))  # type: ignore
            for j in range(num_freq_cfgs):
                if f.readline().strip() != 'CONFIGURATION':
                    raise Exception('LSDalton / OpenRSP file is incomplete.')
                rsp_tensor['properties'][i]['values'].append(  # type: ignore
                    np.zeros(shape=shape, dtype=np.float64))
                for _ in range(max_loop):
                    indexes = tuple([int(index) - 1 for index in f.readline().split()])
                    rsp_tensor['properties'][i]['values'][j][indexes] = float(f.readline())  # type: ignore
                    if np.prod([index + 1 for index in indexes]) == max_loop:
                        break
    return rsp_tensor
